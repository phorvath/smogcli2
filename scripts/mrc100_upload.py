#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2023 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.


import argparse
import websocket
import sys
import os
import hashlib


def load_credentials(qthfile_path=None):
    if qthfile_path is None:
        qthfile_path = 'qthfile.txt'
    if not os.path.isfile(qthfile_path):
        print("ERROR: File path {} does not exist. Exiting...".format(qthfile_path))
        sys.exit(1)

    with open(qthfile_path) as qthfile:
        qthfile_lines = qthfile.readlines(200)

    if len(qthfile_lines) < 6:
        print("WARNING: Upload credentials missing from the QTH file. Uploading anonymously.")
        return None

    return (qthfile_lines[4].rstrip("\n\r"), qthfile_lines[5].rstrip("\n\r"))


def main(args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-q', metavar='QTH',
                        default='qthfile.txt', help='path to the QTH file with station data')
    parser.add_argument('filenames', nargs='*', help="list of pkts filenames")
    args = parser.parse_args(args)

    credentials = load_credentials(args.q)

    ws = websocket.WebSocket()
    ws.connect("wss://gnd.bme.hu:8070/send")

    if credentials is not None:
        username, pwd = credentials
        password = hashlib.sha512(pwd.encode('UTF-8')).hexdigest()
        
        login = '"username" : "' + username + '", "password" : "' + password + '"\n'
        ws.send(login)
        res = ws.recv()
        if not res.endswith(" logged in"):
            print('WARNING: Login failure: ', res)
        else:
            print(res)

    for filename in args.filenames:
        print("Uploading {}".format(filename))

        try:
            with open(filename, 'r') as pktfile:
                data = pktfile.read()
                ws.send(data)
                print(ws.recv())
        except OSError as err:
            print("OS error: {0}".format(err))

    ws.close()


if __name__ == '__main__':
    main()