#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2023 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.


import argparse
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import json
from pathlib import Path


def plot_waterfall(basename, samp_rate, dark_mode, show=False, colorbar=False):
    wf_width = 512
    avg_count = 64
    samples = np.memmap(basename + ".cf32", mode='r', dtype=np.complex64)
    samp_len = samples.shape[0]
    blk_size = wf_width * avg_count
    nblocks = samp_len // blk_size
    pp = 0
    wf = np.zeros((nblocks, wf_width))
    for b in range(nblocks):
        x = samples[pp:pp + blk_size]
        pp += blk_size
        f, wf[b, :] = signal.welch(
            x, samp_rate, nperseg=wf_width, return_onesided=False)

    wf_flip = np.fft.fftshift(10.0 * np.log10(wf), axes=1)

    rec_len_s = pp / samp_rate

    if dark_mode:
        plt.style.use('dark_background')

    fig, ax1 = plt.subplots(1, 1, figsize=(8, 10))
    im1 = ax1.imshow(wf_flip, aspect='auto', extent=(-samp_rate /
               2000., samp_rate/2000., rec_len_s - 1, 0))
    if colorbar:
        fig.colorbar(im1)
    ax1.set_xlabel('Frequency [kHz]')
    ax1.set_ylabel('Time [s]')
    plt.savefig(basename + ".png")

    if show:
        plt.show()


def main(args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('filenames', nargs='*',
                        help="list of filenames without extension")
    parser.add_argument("--dark", action="store_true",
                    help="dark theme")
    parser.add_argument("--bar", action="store_true",
                    help="show the colorbar")
    parser.add_argument("--show", action="store_true",
                    help="show the image on screen beside saving to PNG")
    
    args = parser.parse_args(args)

    for argx in args.filenames:
        p = Path(argx)
        basename = str(p)
        if p.suffix == '.pkts' or p.suffix == '.cf32' or p.suffix == '.meta':
            basename = basename[:-5]

        print("Processing {}".format(basename))

        samp_rate = 50000.0
        meta_path = Path(basename + ".meta")
        if meta_path.is_file():
            try:
                with meta_path.open() as meta_file:
                    meta = json.load(meta_file)
                    samp_rate = meta["options"]["output_samp_rate"]
            except OSError:
                print("WARNING: Metafile {} cannot be opened, assuming 50 ksps sample rate!".format(
                    str(meta_path)))
        else:
            print("WARNING: Metafile {} not found, assuming 50 ksps sample rate!".format(
                str(meta_path)))

        plot_waterfall(basename, samp_rate=samp_rate, dark_mode=args.dark, colorbar=args.dark, show=args.show)


if __name__ == '__main__':
    main()
