#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2021 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

from __future__ import print_function

import argparse
import json

from matplotlib import pyplot


def main(args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--max-snr', type=int, default=20,
                        help='maximum SNR value')
    parser.add_argument('--counts', action='store_true',
                        help='plot counts instead of percentages')
    parser.add_argument('filenames', metavar='FILE', nargs='*',
                        help='list of pkts filenames')
    args = parser.parse_args(args)

    counts = dict()
    for filename in args.filenames:
        print("Reading {}".format(filename))

        try:
            file = open(filename, 'r')
            for line in file:
                packet = json.loads("{" + line.rstrip("\n\r") + "}")
                code_type = packet['code_type']
                if code_type != 'syncpkt' and packet.get('crc') != True:
                    continue
                data_snr = int(round(packet['data_snr']))
                data_snr = min(max(0, data_snr), args.max_snr)
                if code_type not in counts:
                    counts[code_type] = [0] * (args.max_snr + 1)
                counts[code_type][data_snr] += 1

        except OSError as err:
            print("OS error: {0}".format(err))
            continue

    for code_type in counts.keys():
        bins = counts[code_type]
        total = max(sum(bins), 1)
        if not args.counts:
            bins = [100.0 * x / total for x in bins]
        pyplot.plot(range(args.max_snr + 1), bins,
                    alpha=0.5, label=code_type + " (" + str(total) + " pkts)")
    pyplot.legend()
    pyplot.xlabel("SNR (dB)")
    pyplot.ylabel("pkts" if args.counts else "pkts %")
    pyplot.show()


if __name__ == '__main__':
    main()
