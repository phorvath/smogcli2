/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _WIN32
#include <unistd.h>
#else
#include "getopt/getopt.h"
#endif

#include "coherent.hpp"
#include "decoder.hpp"
#include "satellite.hpp"
#include "tle.hpp"
#include <json/json.h>
#include <algorithm>
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>
#include <random>

static std::atomic<bool> do_exit(false);

static void sighandler(int) { do_exit = true; }

std::map<std::string, int> pkt_coded_len_map = { { "ao40", 650 * 8 + 24 },
                                                 { "ao40short", 333 * 8 + 24 },
                                                 { "ra128", 260 * 8 + 24 },
                                                 { "ra256", 514 * 8 + 24 },
                                                 { "syncpkt", 512 + 16 + 24 } };

void test_detector()
{
    float freq = 0.0f;
    float noise = 0.0f;
    std::mt19937 rng(1);
    std::normal_distribution<float> dist(0.0f, std::sqrt(0.5f));

    std::vector<uint8_t> bytes = { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
                                   0xaa, 0x2d, 0xd4, 0x01, 0x23, 0x45, 0x67,
                                   0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45 };
    std::vector<std::complex<float>> input = gmsk_encode_bytes(bytes, 40);
    for (int i = 0; i < 0; i++)
        input.insert(input.begin(), input[0]);

    for (size_t i = 0; i < input.size(); i++) {
        input[i] *= std::complex<float>(std::cos(freq * i), std::sin(freq * i));
        input[i] += noise * std::complex<float>(dist(rng), dist(rng));
    }

    input[2560] = std::complex<float>(0.98777, -0.16666);

    // input_spb = 40, 8 bytes = 64 bits = 64 * 40 = 2560 samples
    // window = 2048 samples, stride 20 * 40 = 800
    preamble_detector detector(
        window_gaussian(2048, 0.4f), 800, 40, 5, 1024, { 0x2d, 0xd4 }, 5.0f, 2.0f, 4);

    for (size_t i = 0; i + detector.get_stride() <= input.size();
         i += detector.get_stride()) {
        const std::vector<preamble_detector::packet_t>& packets =
            detector.work(input.data() + i);

        for (const preamble_detector::packet_t& packet : packets)
            detector.print_packet(packet);
    }
}

void test_viterbi()
{
    float freq = 0.0f;
    float noise = 0.0f;
    std::mt19937 rng(1);
    std::normal_distribution<float> dist(0.0f, std::sqrt(0.5f));

    std::vector<uint8_t> bytes = { 0x2d, 0xd4 };
    std::vector<std::complex<float>> samples = gmsk_encode_bytes(bytes, 5);

    for (size_t i = 0; i < samples.size(); i++) {
        samples[i] *= std::complex<float>(std::cos(freq * i), std::sin(freq * i));
        samples[i] += noise * std::complex<float>(dist(rng), dist(rng));
    }

    viterbi_demod demod(5);
    demod.demodulate(samples.data(), samples.size(), { 0x2d });
    demod.print_stats();
}

void usage(void)
{
    std::cerr << "SMOG-P/ATL-1 demodulator and decoder\n\n"
                 "Usage: smogp_decode [-options] [filenames]\n"
                 "\t-T runs internal test\n"
                 "\t-b bits per second (default: 1250, 2500, 5000 and 12500 bps)\n"
                 "\t-r sampling rate of input (default: 50000 sps)\n"
                 "\t-t tone detection sensitivity in dB (default: 2.0 dB)\n"
                 "\t-s sync detection sensitivity in dB (default: 1.0 dB)\n"
                 "\t-d data detection sensitivity in dB (default: 0.0 dB)\n"
                 "\t-v enable viterbi demodulator (default: off)\n"
                 "\t-g print raw 650 byte packets for gorgon (default: off)\n"
                 "\t-p write packet files instead of stdout (default: off)\n"
                 "\t-S print raw samples for each packet (default: off)\n"
                 "\t-Y decode only sync packets (default: off)\n"
                 "\t-l limit the search to freq offset (default: 25000 Hz)\n"
                 "\t-h prints this help message\n";
}

void print_gorgon(const uint8_t* input, size_t size, std::ostream& cout)
{
    std::stringstream msg;
    msg << std::setfill('0') << std::hex << std::uppercase;
    for (size_t i = 0; i < size; i++)
        msg << std::setw(2) << static_cast<int>(input[i]);
    msg << " RSSI -132 dBm\n";
    cout << msg.str();
}

bool read_json(Json::Value& root, const std::string& metafilename)
{
    std::ifstream metafile;
    metafile.open(metafilename, std::ifstream::binary);

    if (metafile) {
        Json::CharReaderBuilder rbuilder;
        std::string errs;
        if (!Json::parseFromStream(rbuilder, metafile, &root, &errs)) {
            std::cerr << "WARNING: Failed to parse the metafile " << metafilename
                      << std::endl
                      << errs << std::endl;
            return false;
        } else {
            std::cerr << "INFO: using metafile " << metafilename << std::endl;
        }
        metafile.close();
        return true;
    } else {
        std::cerr << "WARNING: Failed to open the metafile " << metafilename << std::endl;
    }
    return false;
}

double calc_packet_duration(const soft_decoder::packet_t& packet, int data_rate)
{
    return (pkt_coded_len_map.find(packet.type)->second) / float(data_rate);
}

int main(int argc, char* argv[])
{
    std::vector<size_t> input_bps;
    size_t samp_rate = 50000;
    float tone_sensitivity = 2.0f;
    float sync_sensitivity = 1.0f;
    float data_sensitivity = 0.0f;
    bool enable_gorgon = false;
    bool write_packets = false;
    bool enable_viterbi = false;
    bool print_samples = false;
    bool decode_sync_only = false;
    bool calc_sat_pos = true;
    double start_daynum = 0.0;
    float freq_limit = 25000;

    int opt;
    while ((opt = getopt(argc, argv, "b:r:t:s:d:l:gpvTSYh")) != -1) {
        switch (opt) {
        case 'b':
            input_bps.push_back(static_cast<size_t>(std::atol(optarg)));
            break;
        case 'r':
            samp_rate = static_cast<size_t>(std::atol(optarg));
            break;
        case 't':
            tone_sensitivity = std::atof(optarg);
            break;
        case 's':
            sync_sensitivity = std::atof(optarg);
            break;
        case 'd':
            data_sensitivity = std::atof(optarg);
            break;
        case 'l':
            freq_limit = std::atof(optarg);
            break;
        case 'g':
            enable_gorgon = true;
            break;
        case 'p':
            write_packets = true;
            break;
        case 'v':
            enable_viterbi = true;
            break;
        case 'S':
            print_samples = true;
            break;
        case 'Y':
            decode_sync_only = true;
            break;
        case 'T':
            test_detector();
            // test_viterbi();
            return 0;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    if (input_bps.empty())
        input_bps = { 1250, 2500, 5000, 12500 };

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    while (!do_exit && optind < argc) {
        std::string filename = argv[optind++];

        size_t pos = filename.find_last_of('.');
        if (pos == std::string::npos)
            pos = filename.size();

        const std::string basename = filename.substr(0, pos);
        std::string filename2 = basename + ".pkts";

        std::cerr << "INFO: reading " << filename;
        if (write_packets)
            std::cerr << ", writing " << filename2;
        std::cerr << std::endl;

        Json::Value meta_root;
        if (!read_json(meta_root, basename + ".meta")) {
            calc_sat_pos = false;
        } else {
            Json::Value tle_root = meta_root["tle"];
            start_daynum = tle_root.get("daynum", 0.0).asDouble();
            if (start_daynum == 0.0) {
                calc_sat_pos = false;
            }
        }

        file_source source;
        source.open(filename.c_str(), file_source::AUTO);
        if (!source.is_open())
            continue;

        std::ofstream cout;
        if (write_packets) {
            cout.open(filename2.c_str(), std::ofstream::trunc);
        }

        viterbi_demod demod4(4);
        viterbi_demod demod5(5);

        std::vector<preamble_detector> detectors;

        sat_tracker tracker(meta_root);

        int decd_ra128_cnt = 0;
        int decd_ra256_cnt = 0;
        int decd_ao40_cnt = 0;
        int decd_ao40_short_cnt = 0;

        int decd_1250_cnt = 0;
        int decd_2500_cnt = 0;
        int decd_5000_cnt = 0;
        int decd_12500_cnt = 0;
        int decd_25000_cnt = 0;

        int sync_500_cnt = 0;
        int sync_1250_cnt = 0;
        int sync_2500_cnt = 0;
        int sync_5000_cnt = 0;
        int sync_12500_cnt = 0;
        int sync_25000_cnt = 0;
        int sync_50000_cnt = 0;
        int sync_125000_cnt = 0;

        int sync_rx_cnt = 0;
        int sync_ao40_short_cnt = 0;
        int sync_ao40_cnt = 0;
        int sync_ra128_cnt = 0;
        int sync_ra256_cnt = 0;
        int sync_ra512_cnt = 0;
        int sync_ra1024_cnt = 0;
        int sync_ra2048_cnt = 0;

        for (size_t bps : input_bps) {
            std::stringstream msg;
            msg << "WARNING: unsupported sampling rate " << samp_rate << " and bps "
                << bps << " combination!" << std::endl;

            if (samp_rate % bps != 0) {
                std::cerr << msg.str();
                continue;
            }
            size_t input_spb = samp_rate / bps;

            size_t output_spb;
            if (input_spb % 5 == 0)
                output_spb = 5;
            else if (input_spb % 4 == 0)
                output_spb = 4;
            else {
                std::cerr << msg.str();
                continue;
            }

            size_t window_size;
            size_t stride_size;
            if (input_spb < 8) { // 4
                window_size = 256;
                stride_size = 100;
            } else if (input_spb < 16) { // 10
                window_size = 512;
                stride_size = 200;
            } else if (input_spb < 32) { // 20
                window_size = 1024;
                stride_size = 400;
            } else if (input_spb < 64) { // 40, 50
                window_size = 2048;
                stride_size = 800;
            } else {
                std::cerr << msg.str();
                continue;
            }

            if (!signal_downsampler::is_supported(input_spb, output_spb)) {
                std::cerr << msg.str();
                continue;
            }

            size_t max_freq_bin = std::ceil(freq_limit * window_size / samp_rate);

            detectors.emplace_back(window_gaussian(window_size, 0.4f),
                                   stride_size,
                                   input_spb,
                                   output_spb,
                                   max_freq_bin,
                                   std::vector<uint8_t>({ 0xaa, 0x2d, 0xd4 }),
                                   tone_sensitivity,
                                   sync_sensitivity,
                                   653);
        }

        if (detectors.empty()) {
            std::cerr << "WARNING: no detector is available to handle this signal!"
                      << std::endl;
            return 1;
        }

        std::vector<std::complex<float>> input(800);
        soft_decoder decoder(CRC_NONE);

        size_t length = 0;
        auto start = std::chrono::high_resolution_clock::now();

        while (!do_exit) {
            if (source.read_float(input) != 0)
                break;

            for (preamble_detector& detector : detectors) {
                assert(samp_rate % detector.get_input_spb() == 0);
                assert(input.size() % detector.get_stride() == 0);

                size_t spb = detector.get_input_spb();

                for (size_t i = 0; i < input.size(); i += detector.get_stride()) {
                    const std::vector<preamble_detector::packet_t>& packets =
                        detector.work(input.data() + i);

                    for (const preamble_detector::packet_t& packet1 : packets) {
                        size_t header_bits;

                        std::vector<float> bits;
                        if (enable_viterbi && detector.get_output_spb() == 4) {
                            bits = demod4.demodulate(packet1.samples.data(),
                                                     packet1.samples.size(),
                                                     detector.get_sync_bytes());
                            header_bits = 0;
                        } else if (enable_viterbi && detector.get_output_spb() == 5) {
                            bits = demod5.demodulate(packet1.samples.data(),
                                                     packet1.samples.size(),
                                                     detector.get_sync_bytes());
                            header_bits = 0;
                        } else {
                            bits = gmsk_soft_decode2(packet1.samples,
                                                     detector.get_output_spb());
                            header_bits = detector.get_sync_size() * 8;
                        }

                        soft_decoder::packet_t packet2 =
                            decoder.work_smog1(bits.data() + header_bits,
                                               bits.size() - header_bits,
                                               decode_sync_only);

                        if (packet2.snr >= data_sensitivity) {
                            std::stringstream msg;
                            detector.print_packet(samp_rate, packet1, msg);
                            msg << ", ";
                            decoder.print_packet(packet2, msg);
                            if (calc_sat_pos) {
                                double begin_daynum =
                                    packet1.position / 86400.0 / double(samp_rate) +
                                    start_daynum;
                                double duration =
                                    calc_packet_duration(packet2, int(samp_rate / spb));
                                msg << ", ";
                                tracker.print_packet(begin_daynum, duration / 86400, msg);
                            }
                            if (print_samples) {
                                msg << ", ";
                                detector.print_samples(packet1, msg);
                            }
                            msg << std::endl;
                            if (!enable_gorgon)
                                (write_packets ? cout : std::cout) << msg.str();

                            if (!std::strcmp(packet2.type, "ra128"))
                                ++decd_ra128_cnt;
                            else if (!std::strcmp(packet2.type, "ra256"))
                                ++decd_ra256_cnt;
                            else if (!std::strcmp(packet2.type, "ao40"))
                                ++decd_ao40_cnt;
                            else if (!std::strcmp(packet2.type, "ao40short"))
                                ++decd_ao40_short_cnt;

                            if (std::strcmp(packet2.type, "syncpkt")) {
                                switch (size_t(samp_rate / spb)) {
                                case 1250:
                                    ++decd_1250_cnt;
                                    break;
                                case 2500:
                                    ++decd_2500_cnt;
                                    break;
                                case 5000:
                                    ++decd_5000_cnt;
                                    break;
                                case 12500:
                                    ++decd_12500_cnt;
                                    break;
                                case 25000:
                                    ++decd_25000_cnt;
                                    break;
                                }
                            }

                            if (!std::strcmp(packet2.type, "syncpkt")) {
                                switch (packet2.data[1] / 8) {
                                case 0:
                                    ++sync_500_cnt;
                                    break;
                                case 1:
                                    ++sync_1250_cnt;
                                    break;
                                case 2:
                                    ++sync_2500_cnt;
                                    break;
                                case 3:
                                    ++sync_5000_cnt;
                                    break;
                                case 4:
                                    ++sync_12500_cnt;
                                    break;
                                case 5:
                                    ++sync_25000_cnt;
                                    break;
                                case 6:
                                    ++sync_50000_cnt;
                                    break;
                                case 7:
                                    ++sync_12500_cnt;
                                    break;
                                }

                                switch (packet2.data[1] % 8) {
                                case 0:
                                    ++sync_rx_cnt;
                                    break;
                                case 1:
                                    ++sync_ao40_short_cnt;
                                    break;
                                case 2:
                                    ++sync_ao40_cnt;
                                    break;
                                case 3:
                                    ++sync_ra128_cnt;
                                    break;
                                case 4:
                                    ++sync_ra256_cnt;
                                    break;
                                case 5:
                                    ++sync_ra512_cnt;
                                    break;
                                case 6:
                                    ++sync_ra1024_cnt;
                                    break;
                                case 7:
                                    ++sync_ra2048_cnt;
                                    break;
                                }
                            }
                        }

                        if (enable_gorgon && bits.size() >= 652 * 8) {
                            std::vector<uint8_t> bytes = soft_to_hard(bits);
                            print_gorgon(bytes.data() + detector.get_sync_size(),
                                         650,
                                         write_packets ? cout : std::cout);
                        }
                    }
                }
            }

            length += input.size();
        }

        double elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(
                             std::chrono::high_resolution_clock::now() - start)
                             .count();

        source.close();
        if (cout.is_open())
            cout.close();

        size_t tone_detected = 0;
        size_t sync_detected = 0;
        for (preamble_detector& detector : detectors) {
            tone_detected += detector.get_tone_detected();
            sync_detected += detector.get_sync_detected();
        }

        std::cerr << "INFO: finished " << filename << ", samples: " << length
                  << ", speed: " << (length / elapsed * 1e-6)
                  << " msps, tone detected: " << tone_detected
                  << " pkts, sync detected: " << sync_detected << " pkts" << std::endl;

        {
            std::stringstream msg;
            msg << "INFO: decoded data packets: "
                << decd_ra128_cnt + decd_ra256_cnt + decd_ao40_cnt + decd_ao40_short_cnt;
            if (decd_ra128_cnt)
                msg << ", RA128: " << decd_ra128_cnt;
            if (decd_ra256_cnt)
                msg << ", RA256: " << decd_ra256_cnt;
            if (decd_ao40_cnt)
                msg << ", AO40: " << decd_ao40_cnt;
            if (decd_ao40_short_cnt)
                msg << ", AO40 short: " << decd_ao40_short_cnt;
            if (decd_1250_cnt)
                msg << ", 1250 bps: " << decd_1250_cnt;
            if (decd_2500_cnt)
                msg << ", 2500 bps: " << decd_2500_cnt;
            if (decd_5000_cnt)
                msg << ", 5000 bps: " << decd_5000_cnt;
            if (decd_12500_cnt)
                msg << ", 12500 bps: " << decd_12500_cnt;
            if (decd_25000_cnt)
                msg << ", 25000 bps: " << decd_25000_cnt;
            msg << std::endl;
            std::cerr << msg.str();
        }

        {
            std::stringstream msg;
            msg << "INFO: decoded sync packets: "
                << sync_500_cnt + sync_1250_cnt + sync_2500_cnt + sync_5000_cnt +
                       sync_12500_cnt + sync_25000_cnt + sync_50000_cnt + sync_125000_cnt;
            if (sync_500_cnt)
                msg << ", 500 bps: " << sync_500_cnt;
            if (sync_1250_cnt)
                msg << ", 1250 bps: " << sync_1250_cnt;
            if (sync_2500_cnt)
                msg << ", 2500 bps: " << sync_2500_cnt;
            if (sync_5000_cnt)
                msg << ", 5000 bps: " << sync_5000_cnt;
            if (sync_12500_cnt)
                msg << ", 12500 bps: " << sync_12500_cnt;
            if (sync_25000_cnt)
                msg << ", 25000 bps: " << sync_25000_cnt;
            if (sync_50000_cnt)
                msg << ", 50000 bps: " << sync_50000_cnt;
            if (sync_125000_cnt)
                msg << ", 125000 bps: " << sync_125000_cnt;
            if (sync_ao40_short_cnt)
                msg << ", AO40 short: " << sync_ao40_short_cnt;
            if (sync_ao40_cnt)
                msg << ", AO40: " << sync_ao40_cnt;
            if (sync_ra128_cnt)
                msg << ", RA128: " << sync_ra128_cnt;
            if (sync_ra256_cnt)
                msg << ", RA258: " << sync_ra256_cnt;
            if (sync_ra512_cnt)
                msg << ", RA512: " << sync_ra512_cnt;
            if (sync_ra1024_cnt)
                msg << ", RA1024: " << sync_ra1024_cnt;
            if (sync_ra2048_cnt)
                msg << ", RA2048: " << sync_ra2048_cnt;
            if (sync_rx_cnt)
                msg << ", receiving: " << sync_rx_cnt;
            msg << std::endl;
            std::cerr << msg.str();
        }

        if (optind < argc)
            std::cerr << std::endl;
    }

    if (do_exit)
        std::cerr << "INFO: aborted by signal" << std::endl;

    return 0;
}
