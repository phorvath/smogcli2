/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "ao40/ao40_decode_message.h"
#include "ao40/ao40_enc.h"
#include "ao40/ao40short_decode_message.h"
#include "ao40/ao40short_enc.h"
#include "decoder.hpp"
#include "ra/ra_config.h"
#include "ra/ra_decoder_gen.h"
#include "ra/ra_encoder.h"
#include <cassert>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>

std::vector<uint8_t> soft_decoder::sync1_bytes = {
    0x97, 0xfd, 0xd3, 0x7b, 0x0f, 0x1f, 0x6d, 0x08, 0xf7, 0x83, 0x5d, 0x9e, 0x59,
    0x82, 0xc0, 0xfd, 0x1d, 0xca, 0xad, 0x3b, 0x5b, 0xeb, 0xd4, 0x93, 0xe1, 0x4a,
    0x04, 0xd2, 0x28, 0xdd, 0xf9, 0x01, 0x53, 0xd2, 0xe6, 0x6c, 0x5b, 0x25, 0x65,
    0x31, 0xc5, 0x7c, 0xe7, 0xf1, 0x38, 0x61, 0x2d, 0x5c, 0x03, 0x3a, 0xc6, 0x88,
    0x90, 0xdb, 0x8c, 0x8c, 0x42, 0xf3, 0x51, 0x75, 0x43, 0xa0, 0x83, 0x93
}; // 64 bytes

std::vector<uint8_t> soft_decoder::sync2_bytes = {
    0xa3, 0x9e, 0x1a, 0x55, 0x6b, 0xcb, 0x5c, 0x2f, 0x2a, 0x5c, 0xad, 0xd5, 0x32,
    0xfe, 0x85, 0x1d, 0xdc, 0xe8, 0xbc, 0xe5, 0x13, 0x7e, 0xba, 0xbd, 0x9d, 0x44,
    0x31, 0x51, 0x3c, 0x92, 0x26, 0x6c, 0xf3, 0x68, 0x98, 0xda, 0xa3, 0xba, 0x7f,
    0x84, 0x86, 0x32, 0x95, 0xac, 0x8d, 0x4e, 0x66, 0x8b, 0x7f, 0x7b, 0xe0, 0x14,
    0xe2, 0x3c, 0x49, 0x45, 0x32, 0xe4, 0x5c, 0x44, 0xf5, 0x6d, 0x2d, 0x0a
}; // 64 bytes

std::vector<uint8_t> soft_decoder::sync3_bytes = {
    0xe0, 0x7f, 0x06, 0x9a, 0x24, 0xe2, 0x57, 0x5b, 0x37, 0x7e, 0x39, 0xc9, 0x4b,
    0xc0, 0x7a, 0xee, 0x13, 0xfb, 0xfa, 0x9e, 0xf8, 0x37, 0x13, 0x44, 0xe2, 0x2b,
    0x04, 0x13, 0x20, 0xc0, 0x24, 0xf9, 0xb5, 0x92, 0x0f, 0x1e, 0xb7, 0x4f, 0x54,
    0xc5, 0x9d, 0x16, 0x73, 0xd0, 0x22, 0x45, 0x07, 0xd2, 0x4b, 0xc2, 0x47, 0xfa,
    0x85, 0x21, 0xa7, 0x0c, 0xb6, 0xbc, 0x0b, 0x74, 0x61, 0xcd, 0xeb, 0xf6
}; // 64 bytes

std::vector<uint8_t> soft_decoder::sync4_bytes = {
    0xfd, 0x43, 0xe8, 0xd3, 0x62, 0x0c, 0xec, 0x29, 0xad, 0x3f, 0x6e, 0x17, 0x4d, 0x09,
    0x46, 0x65, 0xee, 0xe7, 0x50, 0x1d, 0xaa, 0x61, 0x8f, 0x62, 0xd2, 0x91, 0x43, 0xab,
    0x2d, 0x82, 0x93, 0x12, 0x01, 0x50, 0xd5, 0x74, 0x1f, 0x31, 0xe1, 0xb3, 0x63, 0xd0,
    0x78, 0xe4, 0x34, 0x07, 0xcc, 0x78, 0x03, 0x7f, 0xd8, 0x95, 0x23, 0x88, 0xfa, 0x99,
    0x83, 0x89, 0x6d, 0x77, 0x74, 0xff, 0xb8, 0xbc, 0x4b, 0x01, 0xec, 0xe2, 0x82, 0x59,
    0x17, 0x16, 0x07, 0x2f, 0xfd, 0xdd, 0x6a, 0x3b, 0xf9, 0x90, 0x35, 0x83, 0x3a, 0xfa,
    0x2b, 0x4c, 0xc9, 0x1b, 0x6c, 0x8c, 0x7b, 0x4a, 0x26, 0x8a, 0xe0, 0x69, 0x68, 0x10,
    0x69, 0x31, 0x40, 0xae, 0xff, 0xe3, 0x1b, 0x94, 0x84, 0x4a, 0x20, 0x68, 0x19, 0x34,
    0xbd, 0x52, 0x47, 0x19, 0x48, 0x31, 0x42, 0xd4, 0xf8, 0x7e, 0x5b, 0xe3, 0x68, 0xe3,
    0xca, 0x3e, 0x8f, 0x0d, 0x5c, 0xaf, 0x03, 0xc8, 0xd6, 0x19, 0x4d, 0x19, 0x59, 0x0e,
    0x1b, 0x01, 0xae, 0xd3, 0x20, 0x08, 0xc9, 0x1b, 0xe7, 0xa2, 0x2c, 0xb7, 0xe8, 0x60,
    0xf7, 0x6f, 0xca, 0xe2, 0xfc, 0xc0, 0x9b, 0x08, 0x42, 0x2a, 0xa4, 0xf4, 0x72, 0x47,
    0xc2, 0x5d, 0x75, 0x54, 0xf7, 0x1d, 0x30, 0x14, 0x93, 0x3c, 0xb6, 0x6f, 0x27, 0xb7,
    0xad, 0xde, 0x6b, 0x73, 0x8a, 0x6d, 0x21, 0x92, 0xbd, 0x2a, 0xc0, 0x62, 0xe3, 0x84,
    0xa6, 0x8c, 0x50, 0x7c, 0xd8, 0x1a, 0xba, 0x90, 0xe7, 0x66, 0x4e, 0xf1, 0xa8, 0x07,
    0x93, 0x48, 0x30, 0x84, 0xdb, 0xa2, 0x97, 0x22, 0xd7, 0x04, 0x2e, 0x0b, 0x6f, 0x35,
    0x3d, 0x18, 0xb1, 0xc8, 0xa5, 0x34, 0x45, 0x94, 0xcb, 0x99, 0x13, 0x3b, 0xb1, 0xd5,
    0x28, 0xaf, 0x29, 0x47, 0xa5, 0x64, 0x3f, 0x3c, 0xd2, 0xe6, 0x3f, 0x2a, 0x55, 0x33
}; // 252 bytes

std::vector<bool> soft_decoder::create_sync_bits(const std::vector<uint8_t>& bytes)
{
    std::vector<bool> bits;

    for (size_t i = 0; i < bytes.size(); i++) {
        for (int j = 7; j >= 0; j--)
            bits.push_back(((bytes[i] >> j) & 0x1) != 0);
    }

    for (size_t i = 0; i < 6 * 8; i++)
        bits.push_back(false);

    return bits;
}

soft_decoder::soft_decoder(check_crc_t check_crc)
    : check_crc(check_crc),
      sync1_code(create_sync_bits(sync1_bytes)),
      sync2_code(create_sync_bits(sync2_bytes)),
      sync3_code(create_sync_bits(sync3_bytes)),
      sync4_code(create_sync_bits(sync4_bytes))
{
}

soft_decoder::packet_t
soft_decoder::work_smog1(const float* input, size_t length, bool sync_only)
{
    packet_t packet;

    if (length >= sync1_code.size()) {
        packet = work_sync(input, sync1_bytes[0], sync1_code);
    }

    if (length >= sync2_code.size()) {
        packet_t packet2 = work_sync(input, sync2_bytes[0], sync2_code);
        update_if_better(packet, packet2);
    }

    if (length >= sync3_code.size()) {
        packet_t packet2 = work_sync(input, sync3_bytes[0], sync3_code);
        update_if_better(packet, packet2);
    }

    if (sync_only)
        return packet;

    // for RA positive soft bit is 0 hard bit, bit order is reversed
    ra_soft.resize(length);
    for (size_t i = 0; i < length; i += 8)
        for (size_t j = 0; j < 8; j++)
            ra_soft[i + 7 - j] = -input[i + j];

    if (length >= 260 * 8) {
        packet_t packet2 = work_ra("ra128", 64);
        update_if_better(packet, packet2);
    }

    if (length >= 514 * 8) {
        packet_t packet2 = work_ra("ra256", 128);
        update_if_better(packet, packet2);
    }

    if (length >= 1028 * 8) {
        packet_t packet2 = work_ra("ra512", 256);
        update_if_better(packet, packet2);
    }

    if (length >= 2050 * 8) {
        packet_t packet2 = work_ra("ra1024", 512);
        update_if_better(packet, packet2);
    }

    if (length >= 4100 * 8) {
        packet_t packet2 = work_ra("ra2048", 1024);
        update_if_better(packet, packet2);
    }

    if (length >= AO40SHORT_CODE_LENGTH * 8) {
        packet_t packet2 = work_ao40(input, true);
        update_if_better(packet, packet2);
    }

    if (length >= AO40_CODE_LENGTH * 8) {
        packet_t packet2 = work_ao40(input, false);
        update_if_better(packet, packet2);
    }

    return packet;
}

soft_decoder::packet_t
soft_decoder::work_mrc100(const float* input, size_t length, bool sync_only)
{
    packet_t packet;

    if (length >= sync4_code.size()) {
        packet = work_sync(input, sync4_bytes[0], sync4_code);
    }

    if (sync_only)
        return packet;

    // for RA positive soft bit is 0 hard bit, bit order is reversed
    ra_soft.resize(length);
    for (size_t i = 0; i < length; i += 8)
        for (size_t j = 0; j < 8; j++)
            ra_soft[i + 7 - j] = -input[i + j];

    if (length >= 252 * 8) {
        packet_t packet2 = work_ra("ra126", 63);
        update_if_better(packet, packet2);
    }

    return packet;
}

soft_decoder::packet_t
soft_decoder::work_sync(const float* input, uint8_t first_byte, std::vector<bool>& code)
{
    assert(code.size() > 6 * 8);

    unsigned int data = 0;
    for (unsigned int i = 0; i < 6; i++) {
        size_t k = code.size() - 6 * 8;
        float a = 0.0f;
        for (unsigned int j = 0; j < 8; j++)
            a += input[k + 8 * i + j];

        data = (data << 1) | (a >= 0.0f ? 0x1 : 0x0);

        for (unsigned int j = 0; j < 8; j++)
            code[k + 8 * i + j] = data & 0x1;
    }

    packet_t packet;
    packet.type = "syncpkt";
    packet.data.resize(2);
    packet.data[0] = first_byte;
    packet.data[1] = data;
    calculate_ber_snr(input, code, packet);
    packet.crc = packet.ber < 0.05f;

    return packet;
}

soft_decoder::packet_t soft_decoder::work_ra(const char* type, size_t words)
{
    assert(words <= RA_MAX_DATA_LENGTH);

    ra_length_init(words);

    ra_data.resize(ra_data_length);
    ra_decoder_gen(ra_soft.data(), ra_data.data(), 40);

    ra_hard.resize(static_cast<size_t>(ra_code_length) * 16);
    ra_encoder_init(ra_data.data());
    for (size_t i = 0; i < ra_code_length; i++) {
        uint16_t word = ra_encoder_next();
        for (size_t j = 0; j < 16; j++) {
            bool b = (word & (1 << j)) != 0;
            ra_hard[i * 16 + j] = !b;
        }
    }

    packet_t packet;
    packet.type = type;
    packet.data.resize(ra_data.size() * 2);
    std::memcpy(packet.data.data(), ra_data.data(), packet.data.size());
    calculate_ber_snr(ra_soft.data(), ra_hard, packet);
    if (check_crc == CRC_SMOG1)
        calculate_crc_smog1(packet);
    else if (check_crc == CRC_MRC100)
        calculate_crc_mrc100(packet);
    else if (check_crc == CRC_MRC100_SBAND)
        calculate_crc_mrc100_sband(packet);
    return packet;
}

soft_decoder::packet_t soft_decoder::work_ao40(const float* input, bool is_short)
{
    ao40_soft.resize(is_short ? AO40SHORT_RAW_SIZE : AO40_RAW_SIZE);

    float a = 0.0f;
    for (size_t i = 0; i < ao40_soft.size(); i++)
        a += std::abs(input[i]);
    a /= ao40_soft.size();
    a = 32.0f / a;

    for (size_t i = 0; i < ao40_soft.size(); i++)
        ao40_soft[i] = std::min(std::max(a * input[i] + 127.5f, 0.0f), 255.0f);

    ao40_data.resize(is_short ? AO40SHORT_DATA_SIZE : AO40_DATA_SIZE);
    int8_t error[2];
    if (is_short)
        ao40short_decode_data(ao40_soft.data(), ao40_data.data(), error);
    else
        ao40_decode_data(ao40_soft.data(), ao40_data.data(), error);

    ao40_code.resize(is_short ? AO40SHORT_CODE_LENGTH : AO40_CODE_LENGTH);
    if (is_short)
        encode_data_ao40short(ao40_data.data(), ao40_code.data());
    else
        encode_data_ao40(ao40_data.data(), ao40_code.data());

    assert((ao40_soft.size() + 7) / 8 == ao40_code.size());
    ao40_hard.resize(ao40_soft.size());
    for (size_t i = 0; i < ao40_hard.size(); i++) {
        uint8_t b = 1 << (7 - (i % 8));
        ao40_hard[i] = (ao40_code[i / 8] & b) != 0;
    }

    packet_t packet;
    packet.type = is_short ? "ao40short" : "ao40";
    packet.data = ao40_data;
    calculate_ber_snr(input, ao40_hard, packet);
    if (check_crc == CRC_SMOG1)
        calculate_crc_smog1(packet);
    return packet;
}

void soft_decoder::print_packet(const packet_t& packet, std::stringstream& msg) const
{
    msg << std::setprecision(3) << std::fixed << "\"code_type\": \"" << packet.type
        << "\", \"data_snr\": " << packet.snr << ", \"data_ber\": " << packet.ber
        << ", \"crc\": " << (packet.crc ? "true" : "false") << ", \"data\": \""
        << std::setfill('0') << std::hex << std::uppercase;

    for (size_t i = 0; i < packet.data.size(); i++)
        msg << std::setw(2) << static_cast<int>(packet.data[i]);

    msg << "\"" << std::dec;
}

void soft_decoder::print_packet(const packet_t& packet) const
{
    std::stringstream msg;
    print_packet(packet, msg);
    msg << std::endl;
    std::cout << msg.str();
}

void soft_decoder::calculate_ber_snr(const float* input,
                                     const std::vector<bool>& code,
                                     packet_t& packet)
{
    if (!code.empty()) {
        float a = 0.0f;
        for (size_t i = 0; i < code.size(); i++)
            a += code[i] ? input[i] : -input[i];
        a /= code.size(); // average signal amplitude
        a = std::abs(a);  // should be positive

        float b = 0.0f;
        float n = 1e-40f;
        for (size_t i = 0; i < code.size(); i++) {
            b += (input[i] >= 0.0f) == code[i] ? 0.0f : 1.0f;
            float d = input[i] - (code[i] ? a : -a);
            n += d * d;
        }

        packet.ber = b / code.size();
        packet.snr = 10.0f * std::log10(code.size() * a * a / n);
    } else {
        packet.ber = 1.0f;
        packet.snr = -99.0f;
    }
}

void soft_decoder::calculate_crc_smog1(packet_t& packet)
{
    uint16_t crc = 0;
    for (size_t i = 0; i < packet.data.size(); i++) {
        // we do not reverse the bits, so 0x8005 becomes 0xa001
        crc ^= packet.data[i];

        for (int j = 0; j < 8; j++) {
            if ((crc & 0x0001) != 0) {
                crc = (crc >> 1) ^ 0xa001;
            } else {
                crc = crc >> 1;
            }
        }
    }
    packet.crc = crc == 0;
}

static const uint16_t mrc100_crc16_table[] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129,
    0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 0x1231, 0x0210, 0x3273, 0x2252,
    0x52b5, 0x4294, 0x72f7, 0x62d6, 0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c,
    0xf3ff, 0xe3de, 0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 0x3653, 0x2672,
    0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 0xb75b, 0xa77a, 0x9719, 0x8738,
    0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861,
    0x2802, 0x3823, 0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 0xdbfd, 0xcbdc,
    0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 0x6ca6, 0x7c87, 0x4ce4, 0x5cc5,
    0x2c22, 0x3c03, 0x0c60, 0x1c41, 0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b,
    0x8d68, 0x9d49, 0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 0x9188, 0x81a9,
    0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 0x1080, 0x00a1, 0x30c2, 0x20e3,
    0x5004, 0x4025, 0x7046, 0x6067, 0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c,
    0xe37f, 0xf35e, 0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 0x34e2, 0x24c3,
    0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 0xa7db, 0xb7fa, 0x8799, 0x97b8,
    0xe75f, 0xf77e, 0xc71d, 0xd73c, 0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676,
    0x4615, 0x5634, 0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 0xcb7d, 0xdb5c,
    0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 0x4a75, 0x5a54, 0x6a37, 0x7a16,
    0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b,
    0x9de8, 0x8dc9, 0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 0x6e17, 0x7e36,
    0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

uint16_t mrc100_crc16_update(uint16_t chksum_tmp, uint8_t byte)
{
    chksum_tmp =
        (chksum_tmp << 8) ^ mrc100_crc16_table[((chksum_tmp >> 8) ^ byte) & 0xFF];

    return chksum_tmp;
}

void soft_decoder::calculate_crc_mrc100(packet_t& packet)
{
    if (packet.data.size() < 3)
        return;

    uint16_t crc = 0xFFFF;
    crc = mrc100_crc16_update(crc, packet.data[0]);
    for (size_t i = 3; i < packet.data.size(); i++)
        crc = mrc100_crc16_update(crc, packet.data[i]);

    uint8_t high = (crc >> 8) & 0xff;
    uint8_t low = crc & 0xff;

    packet.crc = (packet.data[1] == low) && (packet.data[2] == high);
}

void soft_decoder::calculate_crc_mrc100_sband(packet_t& packet)
{
    if (packet.data.size() < 3)
        return;

    uint16_t crc = 0xFFFF;
    crc = mrc100_crc16_update(crc, packet.data[0]);
    crc = mrc100_crc16_update(crc, packet.data[1]);
    for (size_t i = 4; i < packet.data.size(); i++)
        crc = mrc100_crc16_update(crc, packet.data[i]);

    uint8_t high = (crc >> 8) & 0xff;
    uint8_t low = crc & 0xff;

    packet.crc = (packet.data[2] == low) && (packet.data[3] == high);
}

void soft_decoder::update_if_better(packet_t& packet1, const packet_t& packet2)
{
    if ((packet2.crc == packet1.crc && packet2.snr > packet1.snr) ||
        (packet2.crc && !packet1.crc)) {
        packet1 = packet2;
    }
}
