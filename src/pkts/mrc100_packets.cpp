/*
 * Packet interpreter for MRC-100 pocketqube satellite
 * Copyright (C) 2023 szlldm, Peter Horvath
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef __GNUC__
// disable warnings
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#pragma GCC diagnostic ignored "-Wunused-result"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <json/json.h>

#include <cassert>
#include <cmath>
#include <cstdbool>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <unistd.h>

enum downlink_packet_type {
    DOWNLINK_PCKT_TELEMETRY1 = 1,
    DOWNLINK_PCKT_TELEMETRY2,
    DOWNLINK_PCKT_TELEMETRY3,
    DOWNLINK_PCKT_TELEMETRY4,
    DOWNLINK_PCKT_TELEMETRY5,
    DOWNLINK_PCKT_TELEMETRY6,
    DOWNLINK_PCKT_TELEMETRY_CUSTOM,

    DOWNLINK_PCKT_ADCS_APP_TELEMETRY,
    DOWNLINK_PCKT_BEACON,

    DOWNLINK_PCKT_UPLINK_FEEDBACK,

    DOWNLINK_PCKT_PROGMAP,

    DOWNLINK_PCKT_SPA1,
    DOWNLINK_PCKT_SPA1_TRUNCATED,
    DOWNLINK_PCKT_SPA2,
    DOWNLINK_PCKT_SPA2_TRUNCATED,

    DOWNLINK_PCKT_FILEINFO,

    DOWNLINK_PCKT_FILEDL_STARTBLOCK,
    DOWNLINK_PCKT_FILEDL
};

typedef struct __attribute__((__packed__)) {
    uint32_t file_id;
    uint8_t file_type;
    int32_t file_size;
    int32_t file_close_time;
} downlink_pckt_fileinfo_entry_t;

enum lsw_indexes {
    LSW_INDEX_PCU1 = 0,
    LSW_INDEX_PCU2,
    LSW_INDEX_SDC1,
    LSW_INDEX_SDC2,
    LSW_INDEX_OBC1,
    LSW_INDEX_OBC2,
    LSW_INDEX_SPA2,
    LSW_INDEX_SPA1,
    LSW_INDEX_UCOM1,
    LSW_INDEX_UCOM2,
    LSW_INDEX_ADC1,
    LSW_INDEX_ADC2,
    LSW_INDEX_ACCU1,
    LSW_INDEX_ACCU2,
    LSW_INDEX_SUN1,
    LSW_INDEX_SUN2,
    LSW_INDEX_STX1,
    LSW_INDEX_STX2,
    LSW_INDEX_TID1,
    LSW_INDEX_TID2,
    LSW_INDEX_AIS1,
    LSW_INDEX_AIS2,
    LSW_INDEX_GPSCAM1,
    LSW_INDEX_GPSCAM2,
    LSW_INDEX_PASSIVE1,
    LSW_INDEX_PASSIVE2,
    LSW_INDEX_UNIDE,
    LSW_INDEX_UNIGY,
    LSW_INDEX_UNUSED,
    LSW_INDEX_UNISY,
    LSW_INDEX_UNISZ,
    LSW_INDEX_MPPT12,
    LSW_INDEX_MPPT34,

    LSW_FLAG_COUNT
};

#define BUFCPY(var, siz)                  \
    {                                     \
        {                                 \
            if (siz == 1) {               \
                var = *buf;               \
                buf += siz;               \
            } else {                      \
                memcpy(&(var), buf, siz); \
                buf += siz;               \
            }                             \
        };                                \
    }
#define BUFINCR(siz) \
    {                \
        buf += siz;  \
    }

static bool filter_invalid = false;

const char* translate_lsw_name(int x)
{
    switch (x) {
    case LSW_INDEX_PCU1:
        return "PCU1";
    case LSW_INDEX_PCU2:
        return "PCU2";
    case LSW_INDEX_SDC1:
        return "SDC1";
    case LSW_INDEX_SDC2:
        return "SDC2";
    case LSW_INDEX_OBC1:
        return "OBC1";
    case LSW_INDEX_OBC2:
        return "OBC2";
    case LSW_INDEX_SPA1:
        return "SPA1";
    case LSW_INDEX_SPA2:
        return "SPA2";
    case LSW_INDEX_UCOM1:
        return "COM1";
    case LSW_INDEX_UCOM2:
        return "COM2";
    case LSW_INDEX_ADC1:
        return "ADC1";
    case LSW_INDEX_ADC2:
        return "ADC2";
    case LSW_INDEX_ACCU1:
        return "ACCU1";
    case LSW_INDEX_ACCU2:
        return "ACCU2";
    case LSW_INDEX_SUN1:
        return "SUN1";
    case LSW_INDEX_SUN2:
        return "SUN2";
    case LSW_INDEX_STX1:
        return "STX1";
    case LSW_INDEX_STX2:
        return "STX2";
    case LSW_INDEX_TID1:
        return "TID1";
    case LSW_INDEX_TID2:
        return "TID2";
    case LSW_INDEX_AIS1:
        return "AIS1";
    case LSW_INDEX_AIS2:
        return "AIS2";
    case LSW_INDEX_GPSCAM1:
        return "GPSCAM1";
    case LSW_INDEX_GPSCAM2:
        return "GPSCAM2";
    case LSW_INDEX_PASSIVE1:
        return "PASSIVE1";
    case LSW_INDEX_PASSIVE2:
        return "PASSIVE2";
    case LSW_INDEX_UNIDE:
        return "UNIDEB";
    case LSW_INDEX_UNIGY:
        return "UNIGYR";
    case LSW_INDEX_UNISY:
        return "UNISZ2";
    case LSW_INDEX_UNISZ:
        return "UNISZ1";
    case LSW_INDEX_MPPT12:
        return "MPPT12";
    case LSW_INDEX_MPPT34:
        return "MPPT34";
    default:
        return NULL;
    }
}

const char* translate_lsw_flag(int8_t x)
{
    switch (x) {
    case 1:
        return "On";
    case 0:
        return "OFF";
    case -1:
        return "FAIL";
    case -2:
        return "OverCurrent trip-off";
    case -3:
        return "OverVoltage trip-off";
    default:
        return "INVALID VALUE!";
    }
}

const char* translate_uni_name(int x)
{
    switch (x) {
    case 0:
        return "DEB";
    case 1:
        return "GYR";
    case 3:
        return "SZ2";
    case 4:
        return "SZ1";
    default:
        return NULL;
    }
}

static bool Ttc_valid;
static char Ttc[256];
void proc_timestamp(uint8_t tc)
{
    Ttc[0] = 0;
    Ttc_valid = false;
    tc &= 0x7F;
    if (tc < 126) {
        snprintf(Ttc, 255, "\x1b[32m[%hhu seconds ago]\x1b[0m", tc);
        Ttc_valid = true;
        return;
    }
    if (tc == 126) {
        snprintf(Ttc, 255, "\x1b[33m[older than 125 seconds]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc >= 127) {
        snprintf(Ttc, 255, "\x1b[41m\x1b[37m[INVALID value]\x1b[0m");
        return;
    }
}

void proc_3b_timestamp(int d)
{
    uint8_t tc = (d & 0xFF);
    Ttc[0] = 0;
    Ttc_valid = false;
    tc &= 0x07;
    if (tc <= 1) {
        snprintf(Ttc, 255, "\x1b[32m[%hhu seconds ago]\x1b[0m", tc);
        Ttc_valid = true;
        return;
    }
    if (tc == 2) {
        snprintf(Ttc, 255, "\x1b[32m[<=5 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 3) {
        snprintf(Ttc, 255, "\x1b[32m[<=10 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 4) {
        snprintf(Ttc, 255, "\x1b[32m[<=20 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 5) {
        snprintf(Ttc, 255, "\x1b[32m[<=60 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 6) {
        snprintf(Ttc, 255, "\x1b[33m[older than 60 seconds]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    // if (tc = 7)
    {
        snprintf(Ttc, 255, "\x1b[41m\x1b[37m[INVALID value]\x1b[0m");
        return;
    }
}

void proc_5b_timestamp(int d)
{
    uint8_t tc = (d & 0xFF);
    Ttc[0] = 0;
    Ttc_valid = false;
    tc &= 0x1F;
    if (tc <= 25) {
        snprintf(Ttc, 255, "\x1b[32m[%hhu seconds ago]\x1b[0m", tc);
        Ttc_valid = true;
        return;
    }
    if (tc == 26) {
        snprintf(Ttc, 255, "\x1b[32m[<=30 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 27) {
        snprintf(Ttc, 255, "\x1b[32m[<=40 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 28) {
        snprintf(Ttc, 255, "\x1b[32m[<=60 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 29) {
        snprintf(Ttc, 255, "\x1b[32m[<=120 seconds ago]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    if (tc == 30) {
        snprintf(Ttc, 255, "\x1b[33m[older than 120 seconds]\x1b[0m");
        Ttc_valid = true;
        return;
    }
    // if (tc = 31)
    {
        snprintf(Ttc, 255, "\x1b[41m\x1b[37m[INVALID value]\x1b[0m");
        return;
    }
}


#define DECODE_BOOL_FROM_BUF() \
    BUFCPY(u8, 1);             \
    tmpb = (u8 & 0x80);        \
    proc_timestamp(u8);

#define DECODE_U8_FROM_BUF() \
    BUFCPY(u8, 1);           \
    proc_timestamp(u8);      \
    BUFCPY(u8, 1);

#define DECODE_U16_FROM_BUF() \
    BUFCPY(u8, 1);            \
    proc_timestamp(u8);       \
    BUFCPY(u16, 2);

#define DECODE_I16_FROM_BUF() \
    BUFCPY(u8, 1);            \
    proc_timestamp(u8);       \
    BUFCPY(i16, 2);

#define DECODE_U32_FROM_BUF() \
    BUFCPY(u8, 1);            \
    proc_timestamp(u8);       \
    BUFCPY(u32, 4);

#define DECODE_I32_FROM_BUF() \
    BUFCPY(u8, 1);            \
    proc_timestamp(u8);       \
    BUFCPY(i32, 4);

#define DECODE_LSW_FLAG_FROM_BUF() \
    BUFCPY(u8, 1);                 \
    proc_5b_timestamp(u8);         \
    u8 >>= 5;                      \
    u8 &= 0x07;                    \
    u8 = (uint8_t)(((int8_t)u8) - 4);

#define DECODE_LSW_CV_FROM_BUF()   \
    BUFCPY(u32, 4);                \
    proc_5b_timestamp(u32 & 0xFF); \
    tmpb = (u32 & 0x20);           \
    u16c = ((u32 >> 6) & 0x1FFF);  \
    u16v = ((u32 >> 19) & 0x1FFF);

#define DECODE_U16_CVT_FROM_BUF() \
    BUFCPY(u16, 2);               \
    proc_3b_timestamp(u16);       \
    u16 /= 8;

#define DECODE_I16_CVT_FROM_BUF() \
    BUFCPY(i16, 2);               \
    proc_3b_timestamp(i16);       \
    i16 /= 8;

#define tlmprintf(...)                  \
    if (Ttc_valid || !filter_invalid) { \
        printf("\t");                   \
        if (!Ttc_valid)                 \
            printf("\x1b[33m");         \
        printf(__VA_ARGS__);            \
        printf("\x1b[0m");              \
        printf("\x1b[64G%s\n", Ttc);    \
    }

// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_TELEMETRY1(uint8_t* buf)
{
    printf("Telemetry 1\n");
    bool tmpb;
    int32_t i32;
    uint8_t u8;
    uint16_t u16;
    int16_t i16;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    BUFCPY(i32, 4);
    printf("\tUptime: %d\n", i32);

    BUFCPY(u8, 1);
    if (u8 & 0x80)
        printf("\tOBC-ID: INVALID\n");
    else {
        if (u8 & 0x40)
            printf("\tOBC-ID: 2\n");
        else
            printf("\tOBC-ID: 1\n");
    }
    printf("\tOsc status: %d\n", ((u8 & 0x20) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU1 active: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU2 active: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU DSW1 satus: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU DSW2 satus: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU RBF satus: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU Antenna Deployment flag: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("PCU Antenna Deployment status: %d", ((tmpb) ? 1 : 0));

    DECODE_U16_FROM_BUF();
    tlmprintf("Antenna opened condition: %04X", u16);

    DECODE_U16_FROM_BUF();
    tlmprintf("PCU1 boot counter: %hu", u16);
    DECODE_U16_FROM_BUF();
    tlmprintf("PCU2 boot counter: %hu", u16);

    DECODE_U16_FROM_BUF();
    tlmprintf("PCU1 uptime: %hu [minutes]", u16);
    DECODE_U16_FROM_BUF();
    tlmprintf("PCU2 uptime: %hu [minutes]", u16);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("PCU unregulated bus voltage: %hu [mV]", u16);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("PCU regulated bus voltage: %hu [mV]", u16);

    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("PCU temperature: %.1f [C]", i16 / 10.0);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("PCU supply voltage: %hu [mV]", u16);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC1 input current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC1 output current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC1 output voltage: %hu [mV]", u16);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC2 input current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC2 output current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("SDC2 output voltage: %hu [mV]", u16);

    DECODE_BOOL_FROM_BUF();
    tlmprintf("SDC1 overcurrent status: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("SDC2 overcurrent status: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("SDC1 overvoltage status: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("SDC2 overvoltage status: %d", ((tmpb) ? 1 : 0));

    for (int i = 0; i < LSW_FLAG_COUNT; i++) {
        DECODE_LSW_FLAG_FROM_BUF();
        tlmprintf(
            "LSW-%s flag: %s", translate_lsw_name(i), translate_lsw_flag((int8_t)u8));
    }

    DECODE_U8_FROM_BUF();
    tlmprintf("UART2 error: 0x%02hhX", u8);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART3 error: 0x%02hhX", u8);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART4 error: 0x%02hhX", u8);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART6 error: 0x%02hhX", u8);

    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / COM & STX: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / PCU1: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / PCU2: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / MPPT: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / HiBUS: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / SPA1: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / SPA2: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / LoBUS: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / ADCS: %.0f%%", u8 / 2.55);
    DECODE_U8_FROM_BUF();
    tlmprintf("UART error rate / ACCU: %.0f%%", u8 / 2.55);

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_TELEMETRY2(uint8_t* buf)
{
    printf("Telemetry 2\n");
    bool tmpb;
    int32_t i32;
    uint16_t u16c, u16v;
    uint32_t u32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    for (int i = 0; i < LSW_FLAG_COUNT; i++) {
        if ((i >= LSW_INDEX_UNIDE) && (i <= LSW_INDEX_UNISZ))
            continue;
        DECODE_LSW_CV_FROM_BUF();
        tlmprintf("LSW-%s:\t%s\t%hu [mV]\t%hu [mA]",
                  translate_lsw_name(i),
                  (tmpb ? "ON" : "xx"),
                  u16v,
                  u16c);
    }


    return true;
}


// ##############################################################################
// ##############################################################################
const char* translate_sidepanel_index(int x)
{
    if (x == 0)
        return "X-";
    if (x == 1)
        return "X+";
    if (x == 2)
        return "Y-";
    if (x == 3)
        return "Y+";
    if (x == 4)
        return "Z-";
    if (x == 5)
        return "Z+";
    return "INVALID";
}

bool decode_DOWNLINK_PCKT_TELEMETRY3(uint8_t* buf)
{
    printf("Telemetry 3\n");
    bool tmpb;
    int32_t i32;
    uint8_t u8;
    uint16_t u16, u16c, u16v;
    int16_t i16;
    uint32_t u32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    for (int i = LSW_INDEX_UNIDE; i <= LSW_INDEX_UNISZ; i++) {
        if (i == LSW_INDEX_UNUSED)
            continue;
        DECODE_LSW_CV_FROM_BUF();
        tlmprintf("LSW-%s:\t%s\t%hu [mV]\t%hu [mA]",
                  translate_lsw_name(i),
                  (tmpb ? "ON" : "xx"),
                  u16v,
                  u16c);
    }

    BUFCPY(u32, 4);
    printf("\tPCU1 LSW status: %08X\n", u32);
    BUFCPY(u32, 4);
    printf("\tPCU2 LSW status: %08X\n", u32);

    for (int i = 0; i < 5; i++) {
        if (i == 2)
            continue;
        DECODE_BOOL_FROM_BUF();
        tlmprintf("UNIV-%s active: %d", translate_uni_name(i), ((tmpb) ? 1 : 0));
    }

    for (int i = 0; i < 4; i++) {
        DECODE_U16_CVT_FROM_BUF();
        tlmprintf("MPPT-%d input current: %hu [mA]", i + 1, u16);
    }

    for (int i = 0; i < 4; i++) {
        DECODE_U16_CVT_FROM_BUF();
        tlmprintf("MPPT-%d input voltage: %hu [mV]", i + 1, u16);
    }

    for (int i = 0; i < 4; i++) {
        DECODE_U16_CVT_FROM_BUF();
        tlmprintf("MPPT-%d output current: %hu [mA]", i + 1, u16);
    }

    for (int i = 0; i < 4; i++) {
        DECODE_U16_CVT_FROM_BUF();
        tlmprintf("MPPT-%d output voltage: %hu [mV]", i + 1, u16);
    }

    for (int i = 0; i < 6; i++) {
        DECODE_I16_CVT_FROM_BUF();
        tlmprintf(
            "SOLAR %s temperature: %.1f [C]", translate_sidepanel_index(i), i16 / 10.0);
    }

    for (int i = 0; i < 6; i++) {
        DECODE_U16_FROM_BUF();
        double lux = (u16 & 0x7FFF);
        if (u16 & 0x8000) // dark mode
        {
            lux *= (177.0 / 32767.0);
        } else // light mode
        {
            lux *= (181192.0 / 32767.0);
        }
        tlmprintf("ACCU LIGHTSENSOR %s: %.3f [lux] (%s mode)",
                  translate_sidepanel_index(i),
                  lux,
                  ((u16 & 0x8000) ? "dark" : "light"));
    }

    for (int i = 0; i < 6; i++) {
        DECODE_U16_FROM_BUF();
        tlmprintf("ACCU INFRASENSOR %s: %hu [count]", translate_sidepanel_index(i), u16);
    }


    BUFCPY(u32, 4);
    printf("\tLast File-ID: %u\n", u32);

    return true;
}


// ##############################################################################
// ##############################################################################
const char* translate_accutemp_index(int panel, int x)
{
    if (x == 0) {
        if (panel == 0)
            return "X-";
        return "X+";
    }
    if (x == 1)
        return "Y-";
    if (x == 2)
        return "Y+";
    if (x == 3)
        return "Z-";
    if (x == 4)
        return "Z+";
    if (x == 5)
        return "PANEL";
    return "INVALID";
}

bool decode_DOWNLINK_PCKT_TELEMETRY4(uint8_t* buf)
{
    printf("Telemetry 4\n");
    bool tmpb;
    int32_t i32;
    uint8_t u8;
    uint16_t u16;
    int16_t i16;
    uint32_t u32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    DECODE_BOOL_FROM_BUF();
    tlmprintf("ACCU1 active: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("ACCU2 active: %d", ((tmpb) ? 1 : 0));

    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("ACCU1 current: %hd [mA]", i16);
    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("ACCU2 current: %hd [mA]", i16);

    for (int i = 0; i < 6; i++) {
        DECODE_I16_CVT_FROM_BUF();
        tlmprintf(
            "ACCU1 Temperature %s: %.1f [C]", translate_accutemp_index(0, i), i16 / 10.0);
    }
    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("ACCU1 Temperature REF: %hd [uA]", i16);

    for (int i = 0; i < 6; i++) {
        DECODE_I16_CVT_FROM_BUF();
        tlmprintf(
            "ACCU2 Temperature %s: %.1f [C]", translate_accutemp_index(1, i), i16 / 10.0);
    }
    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("ACCU2 Temperature REF: %hd [uA]", i16);

    DECODE_U32_FROM_BUF();
    // ddmmyy
    int dyear = (u32 % 100) + 2000;
    int dmonth = ((u32 / 100) % 100);
    int dday = ((u32 / 10000) % 100);
    tlmprintf("GPS date: %d-%02d-%02d", dyear, dmonth, dday);
    DECODE_U32_FROM_BUF();
    // hhmmsslll
    int thour = ((u32 / 10000000) % 100);
    int tmin = ((u32 / 100000) % 100);
    int tsec = ((u32 / 1000) % 100);
    int tms = (u32 % 1000);
    tlmprintf("GPS time: %02d:%02d:%02d.%03d", thour, tmin, tsec, tms);
    double d;
    DECODE_I32_FROM_BUF();
    d = i32;
    d *= (180.0 / 2147483648.0);
    tlmprintf("GPS Latitude: %.6f [deg]", d);
    DECODE_I32_FROM_BUF();
    d = i32;
    d *= (180.0 / 2147483648.0);
    tlmprintf("GPS Longitude: %.6f [deg]", d);
    DECODE_U32_FROM_BUF();
    tlmprintf("GPS Altitude: %u [m]", u32);
    DECODE_U16_FROM_BUF();
    tlmprintf("GPS Velocity: %hu [m/s]", u16);
    DECODE_U16_FROM_BUF();
    d = u16;
    d /= 10.0;
    tlmprintf("GPS Course: %.3f [deg]", d);

    DECODE_I16_FROM_BUF();
    tlmprintf("Gyroscope omega-P: %hd [deg/s]", i16);
    DECODE_I16_FROM_BUF();
    tlmprintf("Gyroscope omega-R: %hd [deg/s]", i16);
    DECODE_I16_FROM_BUF();
    tlmprintf("Gyroscope omega-Y: %hd [deg/s]", i16);

    DECODE_I16_FROM_BUF();
    tlmprintf("Magnetometer X: %hd [nT]", i16);
    DECODE_I16_FROM_BUF();
    tlmprintf("Magnetometer Y: %hd [nT]", i16);
    DECODE_I16_FROM_BUF();
    tlmprintf("Magnetometer Z: %hd [nT]", i16);


    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("COM temperature: %.1f [C]", i16 / 10.0);

    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("STX temperature: %.1f [C]", i16 / 10.0);

    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("RTCC temperature: %.1f [C]", i16 / 10.0);

    return true;
}

// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_TELEMETRY5(uint8_t* buf)
{
    printf("Telemetry 5\n");
    int32_t i32;
    uint8_t u8;
    int16_t i16;
    char tmy[20];

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    for (int k = 0; k < 5; k++) {
        BUFCPY(i16, 2);
        BUFCPY(u8, 1);
        if ((i16 < 0) || (u8 > 4))
            printf("\t[%d]-telemetry not valid.\n", k);
        else {
            memcpy(tmy, buf, 19);
            tmy[19] = 0;
            printf("\t%s-telemetry : \"%s\" (%hd seconds ago)\n",
                   translate_uni_name(u8),
                   tmy,
                   i16);
        }
        BUFINCR(19);
    }

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_TELEMETRY6(uint8_t* buf)
{
    printf("Telemetry 6\n");
    bool tmpb;
    int32_t i32;
    uint8_t u8;
    uint16_t u16;
    int16_t i16;
    uint32_t u32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    DECODE_BOOL_FROM_BUF();
    tlmprintf("TID1 active: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("TID2 active: %d", ((tmpb) ? 1 : 0));
    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("TID1 temperature: %.1f [C]", i16 / 10.0);
    DECODE_I16_CVT_FROM_BUF();
    tlmprintf("TID2 temperature: %.1f [C]", i16 / 10.0);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("TID1 voltage: %hu [mV]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("TID2 voltage: %hu [mV]", u16);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID1 measurement serial number: %u", u32);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID1 RadFET1: %f [mGray]", (*((float*)(&u32))) / 10.0);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID1 RadFET2: %f [mGray]", (*((float*)(&u32))) / 10.0);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID2 measurement serial number: %u", u32);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID2 RadFET1: %f [mGray]", (*((float*)(&u32))) / 10.0);
    DECODE_U32_FROM_BUF();
    tlmprintf("TID2 RadFET2: %f [mGray]", (*((float*)(&u32))) / 10.0);

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("COM RX current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("COM idle current: %hu [mA]", u16);

    DECODE_U8_FROM_BUF();
    tlmprintf("AIS1 buffered messages: %hu", u8);
    DECODE_U8_FROM_BUF();
    tlmprintf("AIS2 buffered messages: %hu", u8);

    DECODE_BOOL_FROM_BUF();
    tlmprintf("HAM repeater enabled: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS programming enabled: %d", ((tmpb) ? 1 : 0));

    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS active: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS exec: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS erased: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS valid code: %d", ((tmpb) ? 1 : 0));
    DECODE_U16_FROM_BUF();
    tlmprintf("ADCS code CRC-16: 0x%04hX", u16);

    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS SUN1 status: %d", ((tmpb) ? 1 : 0));
    DECODE_BOOL_FROM_BUF();
    tlmprintf("ADCS SUN2 status: %d", ((tmpb) ? 1 : 0));

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_TELEMETRY_CUSTOM(uint8_t* buf)
{
    printf("Telemetry CUSTOM\n");
    int32_t i32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_ADCS_APP_TELEMETRY(uint8_t* buf)
{
    printf("Telemetry ADCS-App\n");
    int32_t i32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    char tmy[112 + 1];
    memcpy(tmy, buf, 112);
    tmy[112] = 0;
    for (int k = 0; k < 112; k++) {
        if (tmy[k] == '\n') {
            tmy[k] = 0;
            break;
        }
    }
    printf("\tADCS-App telemetry : \"%s\"\n", tmy);

    return true;
}

// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_BEACON(uint8_t* buf)
{
    printf("Beacon\n");
    bool tmpb;
    int32_t i32;
    uint8_t u8;
    uint16_t u16;
    uint32_t u32;
    char mcs[5];

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    for (int i = 7; i >= 0; i--) {
        DECODE_U32_FROM_BUF();
        BUFCPY(u8, 1);
        tlmprintf("Serial number -%d: %u\tRSSI: %hhu", i, u32, u8);
    }

    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("COM telemetry TX max current: %hu [mA]", u16);
    DECODE_U16_CVT_FROM_BUF();
    tlmprintf("COM telemetry TX min voltage: %hu [mV]", u16);
    DECODE_BOOL_FROM_BUF();
    tlmprintf("Active COM: %s", (tmpb ? "COM-2" : "COM-1"));

    DECODE_U32_FROM_BUF();
    memcpy(mcs, &u32, 4);
    mcs[4] = 0;
    tlmprintf("Energy management COM TX MCS: %s", mcs);
    DECODE_U8_FROM_BUF();
    tlmprintf("Energy management COM TX power level: %hhu", u8);
    DECODE_U16_FROM_BUF();
    tlmprintf("Energy management COM cycle sleep time: %hu [sec]", u16);
    DECODE_BOOL_FROM_BUF();
    tlmprintf("Energy management emergency mode: %s", (tmpb ? "ACTIVE" : "inactive"));

    char beacon_msg[49];
    for (int i = 0; i < 48; i++) {
        beacon_msg[i] = (char)buf[i];
        if (buf[i] == 0)
            break;
    }
    beacon_msg[48] = 0;
    printf("\tMessage: %s\n", beacon_msg);
    BUFINCR(48);

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_PROGMAP(uint8_t* buf)
{
    printf("ProgMap\n");
    int32_t i32;
    uint8_t u8;
    uint8_t map[4097];
    memset(map, '?', 4096);

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    int mapi = 0;

    for (int i = 0; i < 112; i++) {
        BUFCPY(u8, 1);
        for (int j = 0; j < (u8 & 0x7F); j++) {
            if (mapi >= 4096)
                break;
            map[mapi] = ((u8 & 0x80) ? '#' : '.');
            mapi += 1;
        }
        if (mapi >= 4096)
            break;
    }

    mapi += 16;
    mapi /= 16;
    mapi *= 16;
    if (mapi > 4096)
        mapi = 4096;
    map[mapi] = 0;
    printf("%s\n", map);

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_SPA(uint8_t* buf, int spa_index)
{
    // 13 bytes header (startfreq, stepsize, timestamp, rbw), 1 byte block number, 102
    // sample
    if (spa_index)
        printf("Spectrum-2\n");
    else
        printf("Spectrum-1\n");

    int32_t i32;
    uint8_t u8;
    uint32_t u32;
    double d;
    uint32_t meastime, startfreq, stepsize, rbw;

    BUFCPY(u32, 4);
    d = u32;
    d /= 1000000.0;
    startfreq = u32;
    BUFCPY(u32, 4);
    stepsize = u32;
    printf("\tStart frequency: %f MHz\tStep frequency: %u Hz\n", d, u32);
    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    meastime = (uint32_t)i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);
    BUFCPY(u8, 1);
    rbw = u8;
    printf("\tRBW: %hhu\n", u8);
    BUFCPY(u8, 1);
    printf("\tBlock number: %hhu\n", u8);
    int blocknum = u8;

    printf("\tBins: 102\n");
    const int datacount = 102;

    char filename[256];
    snprintf(filename, 255,
            "SPA%d_%010u_%u_%u_0x%02X.raw",
            (spa_index + 1),
            meastime,
            startfreq,
            stepsize,
            rbw);
    FILE* f = fopen(filename, "rb+");
    if (f == NULL) // not exists, create new
    {
        f = fopen(filename, "wb+");
        fclose(f);
        f = fopen(filename, "rb+");
    }
    if (f != NULL) {
        fseek(f, 0, SEEK_END);
        int filesize = ftell(f);
        fseek(f, 0, SEEK_SET);

        if ((blocknum * 102 + datacount) > filesize) {
            ftruncate(fileno(f), (blocknum * 102 + datacount));
            filesize = (blocknum * 102 + datacount);
        }
        fseek(f, blocknum * 102, SEEK_SET);
        fwrite(buf, 1, (size_t)datacount, f);
        fclose(f);
    }

    return true;
}

// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_SPA_TRUNCATED(uint8_t* buf, int spa_index)
{
    // 13 bytes header (startfreq, stepsize, timestamp, rbw), 1 byte block number, 1 byte
    // sample count, upto 101 sample
    if (spa_index)
        printf("Spectrum-2\n");
    else
        printf("Spectrum-1\n");

    int32_t i32;
    uint8_t u8;
    uint32_t u32;
    double d;
    uint32_t meastime, startfreq, stepsize, rbw;

    BUFCPY(u32, 4);
    d = u32;
    d /= 1000000.0;
    startfreq = u32;
    printf("\tStart frequency: %f MHz\n", d);
    BUFCPY(u32, 4);
    stepsize = u32;
    printf("\tStep frequency: %u Hz\n", u32);
    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    meastime = (uint32_t)i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);
    BUFCPY(u8, 1);
    rbw = u8;
    printf("\tRBW: %hhu\n", u8);
    BUFCPY(u8, 1);
    printf("\tBlock number: %hhu\n", u8);
    int blocknum = u8;

    BUFCPY(u8, 1);
    printf("\tBins: %hhu\n", u8);
    int datacount = u8;

    char filename[256];
    snprintf(filename, 255,
            "SPA%d_%010u_%u_%u_0x%02X.raw",
            (spa_index + 1),
            meastime,
            startfreq,
            stepsize,
            rbw);
    FILE* f = fopen(filename, "rb+");
    if (f == NULL) // not exists, create new
    {
        f = fopen(filename, "wb+");
        fclose(f);
        f = fopen(filename, "rb+");
    }
    if (f != NULL) {
        fseek(f, 0, SEEK_END);
        int filesize = ftell(f);
        fseek(f, 0, SEEK_SET);

        if ((blocknum * 102 + datacount) > filesize) {
            ftruncate(fileno(f), (blocknum * 102 + datacount));
            filesize = (blocknum * 102 + datacount);
        }
        fseek(f, blocknum * 102, SEEK_SET);
        fwrite(buf, 1, (size_t)datacount, f);
        fclose(f);
    }

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_UPLINK_FEEDBACK(uint8_t* buf)
{
    printf("Uplink feedback\n");

    int32_t i32;
    uint32_t u32, pcktcnt;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    BUFCPY(pcktcnt, 4);
    printf("\tReceived uplink packets: %u\n", pcktcnt);

    printf("\tReceived serial numbers: ");
    for (uint32_t i = 0; i < 27; i++) {
        if (i == pcktcnt)
            break;
        BUFCPY(u32, 4);
        printf("%u, ", u32);
    }
    printf("\n");


    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_FILEINFO(uint8_t* buf)
{
    printf("FileInfo\n");

    int32_t i32;

    BUFCPY(i32, 4);
    struct tm tmp_tm;
    time_t tmp_time = i32;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tTimestamp: %s\n", timestr);

    downlink_pckt_fileinfo_entry_t file;

    for (int i = 0; i < 8; i++) {
        memcpy(&file, buf, sizeof(downlink_pckt_fileinfo_entry_t));
        BUFINCR(sizeof(downlink_pckt_fileinfo_entry_t));

        if (file.file_id) {
            printf("\tFile ID: %6u, File type: 0x%02hhu, Size: %d bytes\n",
                   file.file_id,
                   file.file_type,
                   file.file_size);
        }
    }

    return false; // TODO
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_FILEDL_STARTBLOCK(uint8_t* buf)
{
    printf("FileDownload - Start block\n");

    int32_t file_close_time;
    uint8_t file_type;
    uint32_t file_id, file_size;
    uint16_t block_number;
    uint8_t data_count;

    BUFCPY(file_id, 4);
    BUFCPY(file_type, 1);
    file_size = 0;
    BUFCPY(file_size, 3);
    BUFCPY(file_close_time, 4);
    block_number = 0;
    data_count = 104;
    if (file_size < 104)
        data_count = file_size;

    struct tm tmp_tm;
    time_t tmp_time = file_close_time;
    char timestr[20];
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    unsigned int tdl = block_number * 104 + 104;
    printf("\tID=0x%08X,  Type=0x%hhX,  Block number: %hu\tDownloaded data: %u\n",
           file_id,
           file_type,
           block_number,
           tdl);
    printf("\tFile size: %u\tTimestamp: %s\n", file_size, timestr);


    char fn[256];
    snprintf(fn, 255, "%02hhX_%08X_%04hX.filedl", file_type, file_id, block_number);
    FILE* fd = fopen(fn, "wb");
    if (fd) {
        fwrite(buf, data_count, 1, fd);
        fclose(fd);
    }

    return true;
}


// ##############################################################################
// ##############################################################################
bool decode_DOWNLINK_PCKT_FILEDL(uint8_t* buf)
{
    printf("FileDownload\n");

    int32_t file_close_time;
    uint8_t file_type;
    uint32_t file_id, file_size;
    uint16_t block_number;
    uint8_t data_count;
    char timestr[20];

    BUFCPY(file_id, 4);
    BUFCPY(file_type, 1);
    BUFCPY(data_count, 1);
    BUFCPY(block_number, 2);
    if (block_number % 2) {
        BUFCPY(file_size, 4);
    } else {
        BUFCPY(file_close_time, 4);
        struct tm tmp_tm;
        time_t tmp_time = file_close_time;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;
    }

    unsigned int tdl = block_number * 104 + data_count;
    printf("\tID=0x%08X,  Type=0x%hhX,  Block number: %hu\tDownloaded data: %u\n",
           file_id,
           file_type,
           block_number,
           tdl);

    if (block_number % 2) {
        printf("\tFile size: %6u\tTimestamp: ??????\n", file_size);
    } else {
        printf("\tFile size: ??????\tTimestamp: %s\n", timestr);
    }

    char fn[256];
    snprintf(fn, 255, "%02hhX_%08X_%04hX.filedl", file_type, file_id, block_number);
    FILE* fd = fopen(fn, "wb");
    if (fd) {
        fwrite(buf, data_count, 1, fd);
        fclose(fd);
    }

    return true;
}


bool hex2u8(uint8_t* str, uint8_t* num)
{
    int i;
    uint8_t tmp_char;

    *num = 0;
    if (str == NULL)
        return false;
    // '0' == 48
    // '9' == 57
    // 'A' == 65
    // 'F' == 70
    for (i = 0; i < 2; i++) {
        tmp_char = str[i];
        tmp_char -= '0';
        if (tmp_char > 9) {
            tmp_char -= 7; // 'A' - '0' - 7 == 10
            if (tmp_char > 15)
                return false;
        }

        *num = *num << 4;
        *num += tmp_char;
    }

    return true;
}

static const uint16_t crc16_table[] = {
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 0x8108, 0x9129,
    0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 0x1231, 0x0210, 0x3273, 0x2252,
    0x52b5, 0x4294, 0x72f7, 0x62d6, 0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c,
    0xf3ff, 0xe3de, 0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
    0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 0x3653, 0x2672,
    0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 0xb75b, 0xa77a, 0x9719, 0x8738,
    0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861,
    0x2802, 0x3823, 0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
    0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 0xdbfd, 0xcbdc,
    0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 0x6ca6, 0x7c87, 0x4ce4, 0x5cc5,
    0x2c22, 0x3c03, 0x0c60, 0x1c41, 0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b,
    0x8d68, 0x9d49, 0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
    0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 0x9188, 0x81a9,
    0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 0x1080, 0x00a1, 0x30c2, 0x20e3,
    0x5004, 0x4025, 0x7046, 0x6067, 0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c,
    0xe37f, 0xf35e, 0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 0x34e2, 0x24c3,
    0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 0xa7db, 0xb7fa, 0x8799, 0x97b8,
    0xe75f, 0xf77e, 0xc71d, 0xd73c, 0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676,
    0x4615, 0x5634, 0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 0xcb7d, 0xdb5c,
    0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 0x4a75, 0x5a54, 0x6a37, 0x7a16,
    0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b,
    0x9de8, 0x8dc9, 0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
    0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 0x6e17, 0x7e36,
    0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

uint16_t crc16_update(uint16_t chksum_tmp, uint8_t byte)
{
    chksum_tmp = (chksum_tmp << 8) ^ crc16_table[((chksum_tmp >> 8) ^ byte) & 0xFF];
    return chksum_tmp;
}


int main(int argc, char* argv[])
{
    uint8_t pckt[128];

    if (argc < 2) {
        printf("Usage: %s <pkts file>\n", argv[0]);
        exit(-1);
    }

    if (argc > 1) {
        for (int i = 1; i < argc; i++) {
            if (strcmp(argv[i], "-fi") == 0)
                filter_invalid = true;
        }
    }

    std::fstream pktsfile;
    pktsfile.open(argv[1], std::ios::in);
    if (!pktsfile.is_open())  {
        fprintf(stderr, "Unable to open input file %s\n", argv[1]);
        exit(-1);
    }

    Json::Reader json_reader;

    std::string s_line;
    while (getline(pktsfile, s_line))  {
        Json::Value json_val;
        if(json_reader.parse('{' + s_line + '}', json_val))  {
            std::string data_string = json_val["data"].asString();
            if(data_string.length() == 126 * 2)  {
                const char *line = data_string.c_str();
                printf("Packet - ");
                for (int i = 0; i < 126; i++) {
                    if (!hex2u8((uint8_t*)line + i * 2, pckt + i))
                        printf("Hex to U8 error\n");
                }

                uint16_t crc = 0xFFFF;
                crc = crc16_update(crc, pckt[0]);
                for (int i = 3; i < 126; i++) {
                    crc = crc16_update(crc, pckt[i]);
                }
                if (memcmp(&crc, pckt + 1, 2) != 0) {
                    printf("CRC error\n");
                    break;
                }

                switch (pckt[0]) {
                case DOWNLINK_PCKT_TELEMETRY1:
                    decode_DOWNLINK_PCKT_TELEMETRY1(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY2:
                    decode_DOWNLINK_PCKT_TELEMETRY2(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY3:
                    decode_DOWNLINK_PCKT_TELEMETRY3(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY4:
                    decode_DOWNLINK_PCKT_TELEMETRY4(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY5:
                    decode_DOWNLINK_PCKT_TELEMETRY5(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY6:
                    decode_DOWNLINK_PCKT_TELEMETRY6(pckt + 3);
                    break;
                case DOWNLINK_PCKT_TELEMETRY_CUSTOM:
                    decode_DOWNLINK_PCKT_TELEMETRY_CUSTOM(pckt + 3);
                    break;
                case DOWNLINK_PCKT_ADCS_APP_TELEMETRY:
                    decode_DOWNLINK_PCKT_ADCS_APP_TELEMETRY(pckt + 3);
                    break;
                case DOWNLINK_PCKT_BEACON:
                    decode_DOWNLINK_PCKT_BEACON(pckt + 3);
                    break;
                case DOWNLINK_PCKT_PROGMAP:
                    decode_DOWNLINK_PCKT_PROGMAP(pckt + 3);
                    break;
                case DOWNLINK_PCKT_SPA1:
                    decode_DOWNLINK_PCKT_SPA(pckt + 3, 0);
                    break;
                case DOWNLINK_PCKT_SPA1_TRUNCATED:
                    decode_DOWNLINK_PCKT_SPA_TRUNCATED(pckt + 3, 0);
                    break;
                case DOWNLINK_PCKT_SPA2:
                    decode_DOWNLINK_PCKT_SPA(pckt + 3, 1);
                    break;
                case DOWNLINK_PCKT_SPA2_TRUNCATED:
                    decode_DOWNLINK_PCKT_SPA_TRUNCATED(pckt + 3, 1);
                    break;
                case DOWNLINK_PCKT_UPLINK_FEEDBACK:
                    decode_DOWNLINK_PCKT_UPLINK_FEEDBACK(pckt + 3);
                    break;
                case DOWNLINK_PCKT_FILEINFO:
                    decode_DOWNLINK_PCKT_FILEINFO(pckt + 3);
                    break;
                case DOWNLINK_PCKT_FILEDL_STARTBLOCK:
                    decode_DOWNLINK_PCKT_FILEDL_STARTBLOCK(pckt + 3);
                    break;
                case DOWNLINK_PCKT_FILEDL:
                    decode_DOWNLINK_PCKT_FILEDL(pckt + 3);
                    break;
                default:
                    // printf("UNKOWN TYPE\n");
                    break;
                }
            }
        }
    }
    pktsfile.close();
}
