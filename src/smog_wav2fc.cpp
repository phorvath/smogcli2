/*
 * Copyright 2021 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << "<infile1.wav> [<infile2.wav> ...]"
                  << std::endl;
        return -1;
    }

    int argind = 1;

    while (argind < argc) {
        std::string filename = argv[argind++];

        size_t pos = filename.find_last_of('.');
        if (pos == std::string::npos)
            pos = filename.size();
        std::string filename2 = filename.substr(0, pos) + ".cf32";

        std::cerr << "INFO: reading " << filename;
        std::cerr << ", writing " << filename2;
        std::cerr << std::endl;

        file_source src;
        file_sink sink(file_sink::CF32, 50000);

        std::vector<float> inbuf(1024);

        if (!src.open(filename.c_str(), file_source::WAVCS16)) {
            std::cerr << "ERROR: input WAV file is invalid, exiting..." << std::endl;
            return -1;
        }
        sink.open(filename2.c_str());

        for (;;) {
            size_t unread = src.read_float(inbuf);
            sink.write(inbuf);
            if (unread != 0)
                break;
        }

        src.close();
        sink.close();

        std::cerr << "Written " << sink.byte_count() << " bytes." << std::endl;
    }
    return 0;
}
