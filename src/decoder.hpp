/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef DECODER_HPP
#define DECODER_HPP

#include <cstdint>
#include <sstream>
#include <vector>

enum check_crc_t {
    CRC_NONE = 0,
    CRC_SMOG1 = 1,
    CRC_MRC100 = 2,
    CRC_MRC100_SBAND = 3,
};

class soft_decoder
{
public:
    soft_decoder(check_crc_t check_crc);

    struct packet_t {
        const char* type = "error";
        float ber = 1.0f;
        float snr = -99.0f;
        std::vector<uint8_t> data;
        bool crc = false;
    };

    packet_t work_smog1(const float* input, size_t length, bool sync_only);
    packet_t work_mrc100(const float* input, size_t length, bool sync_only);

    void print_packet(const packet_t& packet, std::stringstream& msg) const;
    void print_packet(const packet_t& packet) const;

public:
    static std::vector<uint8_t> sync1_bytes;
    static std::vector<uint8_t> sync2_bytes;
    static std::vector<uint8_t> sync3_bytes;
    static std::vector<uint8_t> sync4_bytes;

protected:
    const check_crc_t check_crc;

    packet_t work_ra(const char* type, size_t words);
    packet_t work_ao40(const float* input, bool is_short);
    packet_t work_sync(const float* input, uint8_t first_byte, std::vector<bool>& code);

    static void calculate_ber_snr(const float* input,
                                  const std::vector<bool>& code,
                                  packet_t& packet);
    static void calculate_crc_smog1(packet_t& packet);
    static void calculate_crc_mrc100(packet_t& packet);
    static void calculate_crc_mrc100_sband(packet_t& packet);
    static void update_if_better(packet_t& packet1, const packet_t& packet2);
    static std::vector<bool> create_sync_bits(const std::vector<uint8_t>& bytes);

    std::vector<bool> sync1_code;
    std::vector<bool> sync2_code;
    std::vector<bool> sync3_code;
    std::vector<bool> sync4_code;

    std::vector<float> ra_soft;
    std::vector<uint16_t> ra_data;
    std::vector<bool> ra_hard;
    std::vector<uint8_t> ao40_soft;
    std::vector<uint8_t> ao40_data;
    std::vector<uint8_t> ao40_code;
    std::vector<bool> ao40_hard;
};

#endif // DECODER_HPP
