/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "buffer.hpp"
#include <cassert>
#include <cstring>
#include <limits>
#include <thread>

buffer_t::buffer_t(size_t readers, size_t size)
    : buffer(size, 0), write_pos(0), unreads(readers, 0), forced_async(false)
{
    assert(size > 0 && size < std::numeric_limits<size_t>::max());
}

size_t buffer_t::write(const void* input, size_t length, bool blocking)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (length > 0) {
        size_t unread = 0;
        for (size_t n : unreads) {
            if (n <= buffer.size())
                unread = std::max(unread, n);
        }

        if (unread >= buffer.size()) {
            if (blocking && !forced_async)
                not_full.wait(lock);
            else
                break;
        } else {
            size_t num = std::min(buffer.size() - write_pos, length);
            num = std::min(buffer.size() - unread, num);
            std::memcpy(buffer.data() + write_pos, input, num);

            input = static_cast<const uint8_t*>(input) + num;
            write_pos += num;
            if (write_pos >= buffer.size())
                write_pos = 0;
            length -= num;

            bool notify = false;
            for (size_t& n : unreads) {
                notify |= n == 0;
                if (n <= buffer.size())
                    n += num;
            }

            if (notify) {
                lock.unlock();
                not_empty.notify_all();
                lock.lock();
            }
        }
    }

    return length;
}

size_t buffer_t::read(size_t reader, void* output, size_t length, bool blocking)
{
    assert(reader <= unreads.size());

    std::unique_lock<std::mutex> lock(mutex);
    while (length > 0) {
        size_t unread = unreads[reader];

        if (unread > buffer.size()) // disabled
            break;
        else if (unread <= 0) {
            if (blocking && !forced_async)
                not_empty.wait(lock);
            else
                break;
        } else {
            size_t read_pos;
            if (write_pos >= unread)
                read_pos = write_pos - unread;
            else
                read_pos = buffer.size() - unread + write_pos;

            size_t num = std::min(buffer.size() - read_pos, length);
            num = std::min(num, unread);
            std::memcpy(output, buffer.data() + read_pos, num);

            unreads[reader] -= num;
            output = static_cast<uint8_t*>(output) + num;
            length -= num;

            if (unread == buffer.size()) {
                bool notify = true;
                for (size_t n : unreads)
                    notify &= n != buffer.size();
                if (notify) {
                    lock.unlock();
                    not_full.notify_all();
                    lock.lock();
                }
            }
        }
    }

    lock.unlock();
    if (length > 0)
        std::memset(output, 0, length);

    return length;
}

void buffer_t::enable(size_t reader, bool enable)
{
    assert(reader < unreads.size());
    std::unique_lock<std::mutex> lock(mutex);

    if (!enable) {
        size_t unread = unreads[reader];
        unreads[reader] = buffer.size() + 1;
        lock.unlock();
        if (unread == 0)
            not_empty.notify_all(); // wake up blocked read
        else if (unread == buffer.size())
            not_full.notify_all();  // wake up blocker write
    } else if (unreads[reader] > buffer.size())
        unreads[reader] = 0;
}

bool buffer_t::is_enabled(size_t reader)
{
    assert(reader < unreads.size());
    std::unique_lock<std::mutex> lock(mutex);
    return unreads[reader] <= buffer.size();
}

void buffer_t::clear()
{
    std::unique_lock<std::mutex> lock(mutex);
    write_pos = 0;
    for (size_t& n : unreads) {
        if (n <= buffer.size())
            n = 0;
    }

    lock.unlock();
    not_full.notify_all(); // wake up blocked write
}

void buffer_t::force_async(bool enable)
{
    std::unique_lock<std::mutex> lock(mutex);
    forced_async = enable;

    if (enable) {
        lock.unlock();
        not_empty.notify_all();
        not_full.notify_all();
    }
}

buffered_file_sink::buffered_file_sink(file_sink::format_t format,
                                       uint32_t sample_rate,
                                       size_t buffer_size)
    : buffer_size(buffer_size),
      buffer(1, std::max(buffer_size, static_cast<std::size_t>(1))),
      sink(format, sample_rate),
      count(0),
      running(false)
{
    buffer.enable(0, false);
}

void buffered_file_sink::open(const char* filename)
{
    if (buffer_size > 0) {
        assert(!running);
        this->filename = filename;
        running = true;
        buffer.clear();
        buffer.enable(0, true);
        thread = std::thread(&buffered_file_sink::worker, this);
    } else {
        sink.open(filename);
    }
}

void buffered_file_sink::close()
{
    if (buffer_size > 0) {
        assert(running);
        running = false;
        buffer.force_async(true);
        thread.join();
        buffer.enable(0, false);
        buffer.force_async(false);
    } else {
        sink.close();
    }
}

bool buffered_file_sink::is_open() const
{
    if (buffer_size > 0)
        return running;
    else
        return sink.is_open();
}

size_t buffered_file_sink::write(const void* input, size_t length)
{
    size_t num;
    if (buffer_size > 0)
        num = buffer.write(input, length, true);
    else
        num = sink.write(input, length);

    count += length - num;
    return num;
}

void buffered_file_sink::worker()
{
    assert(buffer_size > 0);

    assert(!sink.is_open());
    sink.open(filename.c_str());

    std::vector<uint8_t> temp(8192);
    while (running) {
        size_t num = buffer.read(0, temp, true);
        sink.write(temp.data(), temp.size() - num);
    }

    // write out everything in buffer
    for (;;) {
        size_t num = buffer.read(0, temp, false);
        sink.write(temp.data(), temp.size() - num);
        if (num != 0)
            break;
    }

    sink.close();
}

buffered_zmq_sink::buffered_zmq_sink(size_t buffer_size)
    : buffer_size(buffer_size),
      buffer(1, std::max(buffer_size, static_cast<std::size_t>(1))),
      count(0),
      running(false)
{
    buffer.enable(0, false);
}

void buffered_zmq_sink::open(const char* address)
{
    if (buffer_size > 0) {
        assert(!running);
        this->address = address;
        running = true;
        buffer.clear();
        buffer.enable(0, true);
        thread = std::thread(&buffered_zmq_sink::worker, this);
    } else {
        sink.open(address);
    }
}

void buffered_zmq_sink::close()
{
    if (buffer_size > 0) {
        assert(running);
        running = false;
        buffer.force_async(true);
        thread.join();
        buffer.enable(0, false);
        buffer.force_async(false);
    } else {
        sink.close();
    }
}

bool buffered_zmq_sink::is_open() const
{
    if (buffer_size > 0)
        return running;
    else
        return sink.is_open();
}

size_t buffered_zmq_sink::write(const void* input, size_t length)
{
    size_t num;
    if (buffer_size > 0)
        num = buffer.write(input, length, true);
    else
        num = sink.write(input, length);

    count += length - num;
    return num;
}

void buffered_zmq_sink::worker()
{
    assert(buffer_size > 0);

    assert(!sink.is_open());
    sink.open(address.c_str());

    std::vector<uint8_t> temp(8192);
    while (running) {
        size_t num = buffer.read(0, temp, true);
        sink.write(temp.data(), temp.size() - num);
    }

    // write out everything in buffer
    for (;;) {
        size_t num = buffer.read(0, temp, false);
        sink.write(temp.data(), temp.size() - num);
        if (num != 0)
            break;
    }

    sink.close();
}
