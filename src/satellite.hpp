/*
 * Copyright 2019-2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef SATELLITE_HPP
#define SATELLITE_HPP

#include "blocks.hpp"
#include "buffer.hpp"
#include "filter.hpp"
#include <atomic>
#include <complex>
#include <string>
#include <thread>
#include <vector>

class sat_downconv_32
{
public:
    sat_downconv_32(size_t work_length);
    virtual ~sat_downconv_32();

    std::vector<std::complex<float>>& input() { return buffer2; }
    const std::vector<std::complex<float>>& output() const { return buffer9; }
    void work();

    void set_correction(float freq) { sinusoid.set_frequency(freq); }
    float get_correction() const { return sinusoid.get_frequency(); }

    static std::vector<float> filter1_taps;
    static std::vector<float> filter2_taps;
    static std::vector<float> filter3_taps;
    static std::vector<float> filter4_taps;
    static std::vector<float> filter5_taps;

protected:
    std::vector<std::complex<float>> buffer2;
    sinusoid_source_cf32 sinusoid;
    std::vector<std::complex<float>> buffer3;
    multiply_cf32 multiply;
    std::vector<std::complex<float>> buffer4;
    filter_fir_tap12_dec2_vec2_f32 filter1;
    std::vector<std::complex<float>> buffer5;
    filter_fir_tap12_dec2_vec2_f32 filter2;
    std::vector<std::complex<float>> buffer6;
    filter_fir_tap12_dec2_vec2_f32 filter3;
    std::vector<std::complex<float>> buffer7;
    filter_fir_vec2_f32 filter4;
    std::vector<std::complex<float>> buffer8;
    filter_fir_vec2_f32 filter5;
    std::vector<std::complex<float>> buffer9;
};

class sat_downconv_8
{
public:
    sat_downconv_8(size_t work_length);
    virtual ~sat_downconv_8();

    std::vector<std::complex<float>>& input() { return buffer2; }
    const std::vector<std::complex<float>>& output() const { return buffer7; }
    void work();

    void set_correction(float freq) { sinusoid.set_frequency(freq); }
    float get_correction() const { return sinusoid.get_frequency(); }

    static std::vector<float> filter1_taps;
    static std::vector<float> filter2_taps;
    static std::vector<float> filter3_taps;

protected:
    std::vector<std::complex<float>> buffer2;
    sinusoid_source_cf32 sinusoid;
    std::vector<std::complex<float>> buffer3;
    multiply_cf32 multiply;
    std::vector<std::complex<float>> buffer4;
    filter_fir_tap12_dec2_vec2_f32 filter1;
    std::vector<std::complex<float>> buffer5;
    filter_fir_vec2_f32 filter2;
    std::vector<std::complex<float>> buffer6;
    filter_fir_vec2_f32 filter3;
    std::vector<std::complex<float>> buffer7;
};

class sat_recorder
{
public:
    sat_recorder(buffer_t& buffer,
                 size_t reader,
                 uint32_t input_samp_rate,
                 uint32_t output_sample_rate,
                 size_t work_length,
                 size_t file_buffer_length);

    void start(const char* basename);
    void stop();
    bool is_running() const { return running; }

    void set_correction(float freq) { correction = freq; } // in [-0.5, 0.5] range
    float get_correction() const { return correction; }

protected:
    buffer_t& buffer;
    const size_t reader;
    const unsigned int decimation;
    sat_downconv_32 ddc_32;
    sat_downconv_8 ddc_8;
    std::atomic<float> correction;
    std::string basename;
    buffered_file_sink sink;

    std::thread thread;
    std::atomic<bool> running;

    void worker();
};

class samples_dumper
{
public:
    samples_dumper(buffer_t& buffer,
                   size_t reader,
                   uint32_t input_samp_rate,
                   uint32_t output_sample_rate,
                   size_t work_length,
                   size_t file_buffer_length,
                   bool enable_stdout,
                   bool enable_zmq);

    void start();
    void stop();
    bool is_running() const { return running; }

    void set_correction(float freq) { correction = freq; } // in [-0.5, 0.5] range
    float get_correction() const { return correction; }

protected:
    buffer_t& buffer;
    const size_t reader;
    const unsigned int decimation;
    sat_downconv_32 ddc_32;
    sat_downconv_8 ddc_8;
    std::atomic<float> correction;
    const bool enable_stdout;
    buffered_file_sink file_sink;
    const bool enable_zmq;
    buffered_zmq_sink zmq_sink;

    std::thread thread;
    std::atomic<bool> running;

    void worker();
};

#endif // SATELLITE_HPP
