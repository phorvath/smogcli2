#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017-2020 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import math
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import torch
import torch.autograd as autograd


DEVICE = torch.device('cuda') if torch.cuda.is_available() \
    else torch.device('cpu')


def firwin(numtaps, cutoff, window="hamming"):
    """Returns the given bandpass filter."""
    taps = signal.firwin(numtaps, cutoff, window=window)
    taps /= math.sqrt(np.sum(np.square(taps)))
    return torch.from_numpy(taps).to(DEVICE)


def half_to_full(sig, odd=True):
    """Converts a half vector to a full symmetric vector."""
    idx = range(sig.size(0)) + range(sig.size(0) - (2 if odd else 1), -1, -1)
    idx = torch.LongTensor(idx, device=sig.device)
    if isinstance(sig, autograd.Variable):
        idx = autograd.Variable(idx)
    return torch.index_select(sig, 0, idx)


def full_to_half(sig):
    """Converts a full (symmetric) vector to its half."""
    r1 = sig.narrow(0, 0, (sig.size(0) + 1) / 2)
    idx = torch.LongTensor(
        range(sig.size(0) - 1, sig.size(0) / 2 - 1, -1), device=sig.device)
    if isinstance(sig, autograd.Variable):
        idx = autograd.Variable(idx)
    r2 = torch.index_select(sig, 0, idx)
    return (r1 + r2) * 0.5


def pad(sig, left=0, right=0, dim=0):
    assert left >= 0 and right >= 0
    if left <= 0 and right <= 0:
        return sig
    tensors = []
    size = list(sig.size())
    if left > 0:
        size[dim] = left
        zero = torch.zeros(size, dtype=sig.dtype, device=sig.device)
        if isinstance(sig, autograd.Variable):
            zero = autograd.Variable(zero)
        tensors.append(zero)
    tensors.append(sig)
    if right > 0:
        size[dim] = right
        zero = torch.zeros(size, dtype=sig.dtype, device=sig.device)
        if isinstance(sig, autograd.Variable):
            zero = autograd.Variable(zero)
        tensors.append(zero)
    return torch.cat(tensors, dim=dim)


def convolve_rr(vec1, vec2, dim=-1):
    """Calculates the full convolution of real vectors."""
    assert dim < 0 and -dim <= vec1.dim() and -dim <= vec2.dim()
    # print(vec1.size(), vec2.size())
    vec2 = pad(vec2, vec1.size(dim) - 1, vec1.size(dim) - 1, dim=dim)
    vec2 = vec2.unfold(dim, vec1.size(dim), 1)
    if dim == -1:
        vec1 = vec1.unsqueeze(-2)
    else:
        vec1 = vec1.unsqueeze(-1)
        vec1 = vec1.transpose(-1, dim - 1)
    return (vec1 * vec2).sum(-1)


def decimate(sig, rate, dim=0, offset=None):
    """Return the decimated signal so that samples at offset are included."""
    assert rate >= 1
    if offset is None:
        assert sig.size(dim) % 2 == 1
        offset = (sig.size(dim) - 1) / 2
    offset = offset % rate
    if offset != 0:
        sig = sig.narrow(dim, offset, sig.size(dim) - offset)
    return sig.unfold(dim, 1, rate)


def interpolate(sig, rate, dim=0):
    assert rate >= 1
    if rate <= 1:
        return sig
    idx = [0] * ((sig.size(dim) - 1) * rate + 1)
    idx[0::rate] = range(1, sig.size(dim) + 1)
    idx = torch.LongTensor(idx, device=sig.device)
    if isinstance(sig, autograd.Variable):
        idx = autograd.Variable(idx)
    sig = pad(sig, left=1, dim=dim)
    return sig.index_select(dim, idx)


def normalize1(sig, dim=0):
    return sig / torch.sum(sig, dim=dim)


def normalize2(sig, dim=0):
    return sig / torch.sum(sig * sig, dim=dim, keepdim=True).sqrt()


def plot_spectrum(taps, show=True):
    taps = taps / (np.sum(np.square(taps)) ** 0.5)
    if show:
        plt.figure()
    w, h = signal.freqz(taps, worN=2048, whole=False)
    w = w / (2.0 * np.pi)
    plt.plot(w, 20.0 * np.log10(np.abs(h)))
    plt.gca().set_ylim([-80, 20])
    if show:
        plt.show()


def plot_spectrums(taps1, taps2, taps3, taps4, taps5):
    fig, (ax0, ax1) = plt.subplots(ncols=2)

    freq, resp = signal.freqz(
        normalize1(taps1).numpy(), worN=1024, whole=False)
    freq /= 2.0 * np.pi
    ax0.plot(freq, 20.0 * np.log10(np.abs(resp)), label="taps1")
    _, resp = signal.freqz(
        normalize1(taps2).numpy(), worN=1024, whole=False)
    ax0.plot(freq, 20.0 * np.log10(np.abs(resp)), label="taps2")
    _, resp = signal.freqz(
        normalize1(taps3).numpy(), worN=1024, whole=False)
    ax0.plot(freq, 20.0 * np.log10(np.abs(resp)), label="taps3")
    _, resp = signal.freqz(
        normalize1(taps4).numpy(), worN=1024, whole=False)
    ax0.plot(freq, 20.0 * np.log10(np.abs(resp)), label="taps4")
    _, resp = signal.freqz(
        normalize1(taps5).numpy(), worN=1024, whole=False)
    ax0.plot(freq, 20.0 * np.log10(np.abs(resp)), label="taps5")
    ax0.set_ylim([-100, 10])
    ax0.set_xlabel("fs")
    ax0.set_ylabel("dB")
    ax0.set_title("relative responses")
    ax0.legend()

    taps = taps1
    taps = convolve_rr(taps, interpolate(taps2, 2))
    taps = convolve_rr(taps, interpolate(taps3, 4))
    taps = convolve_rr(taps, interpolate(taps4, 8))
    taps = convolve_rr(taps, interpolate(taps5, 16))

    freq, resp = signal.freqz(
        normalize1(taps).numpy(), worN=32768, whole=False)
    freq /= 2.0 * np.pi / 1600.0
    ax1.plot(freq[:1024], 20.0 *
             np.log10(np.abs(resp[:1024])), label="passband")
    stopband = np.sum(np.reshape(np.abs(resp[1024:]), [31, 1024]), axis=0)
    ax1.plot(freq[:1024], 20.0 * np.log10(stopband), label="stopband")
    ax1.set_ylim([-100, 10])
    ax1.set_xlabel("Hz")
    ax1.set_title("total response fs=1.6 MHz")
    ax1.legend()

    plt.show()


def design():
    half1 = autograd.Variable(
        full_to_half(firwin(16, 0.5)), requires_grad=True)

    half2 = autograd.Variable(
        full_to_half(firwin(16, 0.5)), requires_grad=True)

    half3 = autograd.Variable(
        full_to_half(firwin(16, 0.5)), requires_grad=True)


if __name__ == "__main__":
    taps1 = torch.tensor([-0.0019545264,
                          -0.0081854569,
                          -0.0160511227,
                          -0.0123682803,
                          0.0217284272,
                          0.0931647992,
                          0.1810567463,
                          0.2426820045,
                          0.2426820045,
                          0.1810567463,
                          0.0931647992,
                          0.0217284272,
                          -0.0123682803,
                          -0.0160511227,
                          -0.0081854569,
                          -0.0019545264])
    taps2 = torch.tensor([-0.0020597041,
                          -0.0086770742,
                          -0.0172482973,
                          -0.0142027203,
                          0.0200562700,
                          0.0929101250,
                          0.1829995159,
                          0.2462978400,
                          0.2462978400,
                          0.1829995159,
                          0.0929101250,
                          0.0200562700,
                          -0.0142027203,
                          -0.0172482973,
                          -0.0086770742,
                          -0.0020597041])
    taps3 = torch.tensor([-0.0011189412,
                          -0.0074104831,
                          -0.0176916959,
                          -0.0186767921,
                          0.0127722812,
                          0.0883478736,
                          0.1865007221,
                          0.2570669602,
                          0.2570669602,
                          0.1865007221,
                          0.0883478736,
                          0.0127722812,
                          -0.0186767921,
                          -0.0176916959,
                          -0.0074104831,
                          -0.0011189412])
    taps4 = torch.tensor([0.0039787512,
                          0.0019021335,
                          -0.0141966812,
                          -0.0339871236,
                          -0.0190288599,
                          0.0644374411,
                          0.1964761267,
                          0.2992741710,
                          0.2992741710,
                          0.1964761267,
                          0.0644374411,
                          -0.0190288599,
                          -0.0339871236,
                          -0.0141966812,
                          0.0019021335,
                          0.0039787512])
    taps5 = torch.tensor([0.0058157505,
                          0.0198873315,
                          0.0016782359,
                          -0.0402628801,
                          -0.0559862395,
                          0.0273145667,
                          0.2000315019,
                          0.3467768196,
                          0.3467768196,
                          0.2000315019,
                          0.0273145667,
                          -0.0559862395,
                          -0.0402628801,
                          0.0016782359,
                          0.0198873315,
                          0.0058157505])

    plot_spectrums(taps1, taps2, taps3, taps4, taps4)
