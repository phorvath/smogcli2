clear
close all

fpass = 20;

% 1600->800
h1 = firpm(15, [0 2*fpass/1600 800/1600 1], [1 1 0 0], [10, 1])
h2 = firpm(15, [0 2*fpass/800 2*240/800 1], [1 1 0 0], [10, 1])
h3 = firpm(15, [0 2*fpass/400 2*140/400 1], [1 1 0 0], [10, 1])
h4 = firpm(15, [0 2*fpass/200 2*71/200 1], [1 1 0 0], [10 1])
h5 = firpm(61, [0 2*fpass/100 2*25/100 1], [1 1 0 0])

h1s = dsp.FIRDecimator(2, h1);
h2s = dsp.FIRDecimator(2, h2);
h3s = dsp.FIRDecimator(2, h3);
h4s = dsp.FIRDecimator(2, h4);
h5s = dsp.FIRDecimator(2, h5);

FC = dsp.FilterCascade(h1s, h2s, h3s, h4s, h5s);
fvtool(FC,  h1s,  h2s, 'Fs', [1600, 1600, 800])

return 

h2us = zeros(1, 2 * length(h2) - 1);
h2us(1:2:end) = h2;

h3us = zeros(1, 4 * length(h3) - 1);
h3us(1:4:end) = h3;

h4us = zeros(1, 8 * length(h4) - 1);
h4us(1:8:end) = h4;

h5us = zeros(1, 16 * length(h5) - 1);
h5us(1:16:end) = h5;

hcomb = conv(h1, conv(h2us, conv(h3us, conv(h5us, h4us))));
freqz(hcomb, 1, 4096, 1600)




