/*
 * Copyright 2019-2020 Peter Horvath, Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _WIN32
#include <unistd.h>
#else
#include "getopt/getopt.h"
#endif

#include "blocks.hpp"
#include "filter.hpp"
#include "predict.h"
#include "rtl_dongle.hpp"
#include "satellite.hpp"
#include "tle.hpp"
#include <unistd.h>
#include <atomic>
#include <cctype>
#include <chrono>
#include <csignal>
#include <fstream>
#include <iomanip>
#include <iostream>

static std::atomic<bool> do_exit(false);

static const uint32_t TUNING_OFFSET = 55000;
static const uint32_t DEFAULT_SAMPLE_RATE = 1600000;
static const size_t DEFAULT_WORK_LENGTH = 2048;

void usage(void)
{
    std::cerr << "SMOG-P/ATL-1 recorder for RTL2832 based DVB-T receivers\n\n"
                 "Usage: smogp_rtl_rx [-options]\n"
                 "\t-d device_index (default: 0)\n"
                 "\t-T enable bias-T on GPIO PIN 0 (works for rtl-sdr.com v3 dongles)\n"
                 "\t-g tuner gain (default: automatic, NOT RECOMMENDED)\n"
                 "\t-p kalibrate-sdr reported fractional ppm error (default: 0.0)\n"
                 "\t-i track the given primary satellite ID (default: 44832)\n"
                 "\t-F downlink frequency for primary sat (default: 437150000.0 Hz)\n"
                 "\t-j track the give satellite ID as secondary sat (default: 44830)\n"
                 "\t-G downlink frequency for secondary sat (default: 437174500.0 Hz)\n"
                 "\t-k download TLE data from celestrak.com\n"
                 "\t-S use 2 Msps/62.5 ksps mode (default: 1.6 Msps/50 ksps)\n"
                 "\t-O dump downconverted samples to STDOUT in binary cf32 format\n"
                 "\t-b disable the 1/128 rescaling of raw samples\n"
                 "\t-a download active.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-l enable azimuth/elevation logging (default: false)\n"
                 "\t-s disable printing sat and signal statistics (default: false)\n"
                 "\t-B file buffer size, useful for slow SD cards (default: 4 Mb)\n"
                 "\t-e elevation limit for start recording (default: -2 deg)\n"
                 "\t-h prints this help message\n";
}

static void sighandler(int) { do_exit = true; }

void write_json(const Json::Value& root, const std::string& basename)
{
    std::ofstream metafile;
    metafile.open(basename + ".meta", std::ofstream::trunc);
    metafile << root;
    metafile.close();
}

int main(int argc, char* argv[])
{
    bool enable_biastee = false;
    bool update_tle = false;
    int dongle_gain = -1;
    float real_ppm_error = 0.0f;
    long sat1_id = 44832; // SMOG-P
    double sat1_downlink = 437150000;
    long sat2_id = 44830; // ATL-1
    double sat2_downlink = 437174500;
    bool dump_samples = false;
    const char* device = NULL;
    size_t file_buffer_size = 4 * 1024 * 1024;
    float elevation_limit = -2.0f;
    bool log_az_el = false;
    bool print_statistics = true;
    uint32_t sample_rate = DEFAULT_SAMPLE_RATE;
    float scaling = 1.0f / 128.0f;
    std::string tle_file_name("cubesat.txt");
    std::ofstream azel_file_1, azel_file_2;
    std::chrono::steady_clock::time_point start1, start2;

    int opt;
    while ((opt = getopt(argc, argv, "d:g:p:Tki:F:j:G:e:B:OShbals")) != -1) {
        switch (opt) {
        case 'd':
            device = optarg;
            break;
        case 'g':
            dongle_gain = std::atof(optarg) * 10;
            break;
        case 'p':
            real_ppm_error = std::atof(optarg);
            break;
        case 'T':
            enable_biastee = true;
            break;
        case 'k':
            update_tle = true;
            break;
        case 'i':
            sat1_id = std::atol(optarg);
            break;
        case 'F':
            sat1_downlink = std::atof(optarg);
            break;
        case 'j':
            sat2_id = std::atol(optarg);
            break;
        case 'G':
            sat2_downlink = std::atol(optarg);
            break;
        case 'e':
            elevation_limit = std::atof(optarg);
            break;
        case 'O':
            dump_samples = true;
            break;
        case 'S':
            sample_rate = 2000000;
            break;
        case 'b':
            scaling = 1.0f;
            break;
        case 'a':
            tle_file_name = "active.txt";
            break;
        case 'l':
            log_az_el = true;
            break;
        case 's':
            print_statistics = false;
            break;
        case 'B':
            file_buffer_size =
                1024 * 1024 * static_cast<size_t>(std::max(std::atol(optarg), 0L));
            break;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    const uint32_t output_sample_rate = sample_rate / 32;

    uint32_t dongle_center_freq = std::round(sat1_downlink - TUNING_OFFSET);
    int dongle_ppm_error = std::round(real_ppm_error);

    double real_center_freq =
        dongle_center_freq * (1.0 - (dongle_ppm_error - real_ppm_error) * 1e-6);

    sat_tracker tracker({ sat_tracker::sat_t(sat1_id, sat1_downlink),
                          sat_tracker::sat_t(sat2_id, sat2_downlink) },
                        update_tle,
                        tle_file_name);

    buffer_t buffer(3, 16 * 32 * DEFAULT_WORK_LENGTH * sizeof(float));
    std::cerr << "INFO: dongle buffer size: " << buffer.size()
              << ", file buffer size: " << file_buffer_size << std::endl;

    rtl_dongle dongle(buffer, scaling, device);
    if (!dongle.is_opened())
        return 2;

    dongle.set_sampling_rate(sample_rate);
    dongle.set_tuner_gain(dongle_gain);
    dongle.set_bias_tee(enable_biastee);
    dongle.set_center_freq(dongle_center_freq);
    dongle.set_ppm_error(dongle_ppm_error);

    Json::Value options;
    options["device"] = dongle.get_device_name();
    options["gain"] = dongle_gain;
    options["ppm_error"] = real_ppm_error;
    options["file_buffer"] = static_cast<unsigned int>(file_buffer_size);
    options["samp_rate"] = sample_rate;
    options["elevation_limit"] = elevation_limit;
    options["biastee"] = enable_biastee;

    std::cerr << "INFO: real center frequency is " << std::setprecision(1) << std::fixed
              << real_center_freq << " Hz" << std::endl;

    sat_recorder recorder1(buffer,
                           0,
                           sample_rate,
                           output_sample_rate,
                           DEFAULT_WORK_LENGTH,
                           file_buffer_size);
    sat_recorder recorder2(buffer,
                           1,
                           sample_rate,
                           output_sample_rate,
                           DEFAULT_WORK_LENGTH,
                           file_buffer_size);
    sat_recorder dumper(buffer,
                        2,
                        sample_rate,
                        output_sample_rate,
                        DEFAULT_WORK_LENGTH,
                        file_buffer_size);

    dongle.start();

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    if (dump_samples)
        dumper.start("-");

    if (!print_statistics)
        std::cerr << "INFO: printing of satellite and signal statistic are disabled"
                  << std::endl;

    unsigned int delay_count = 0;
    unsigned int last_overruns = 0;
    float last_max_signal = 0.0;
    int last_max_signal_count = 0;
    while (!do_exit) {
        if (++delay_count >= 100)
            delay_count = 0;

        tracker.calculate(print_statistics && (delay_count == 0));

        // this will be overwritten if some of the satellites are above
        float dumper_correction =
            (real_center_freq - 0.5 * (sat1_downlink + sat2_downlink)) / sample_rate;

        if (tracker.is_trackable(0) && sat1_downlink > 0) {
            float correction =
                (real_center_freq - sat1_downlink - tracker.get_doppler(0)) / sample_rate;
            recorder1.set_correction(correction);
            if (tracker.get_elevation(0) >= elevation_limit)
                dumper_correction = correction;

            if (!recorder1.is_running() && tracker.get_elevation(0) >= elevation_limit) {
                std::string basename = tracker.get_filename(0, device);
                if (log_az_el) {
                    azel_file_1.open(basename + ".plog");
                    start1 = std::chrono::steady_clock::now();
                }
                recorder1.start(basename.c_str());
                Json::Value root;
                root["options"] = options;
                tracker.add_json_data(0, root);
                write_json(root, basename);
            } else if (recorder1.is_running() &&
                       tracker.get_elevation(0) < elevation_limit) {
                recorder1.stop();
                azel_file_1.close();
            }

            if (recorder1.is_running() && (delay_count % 50 == 0) && log_az_el) {
                std::chrono::steady_clock::time_point now =
                    std::chrono::steady_clock::now();
                int sample_offset_cnt =
                    output_sample_rate / 1000 *
                    std::chrono::duration_cast<std::chrono::milliseconds>(now - start1)
                        .count();
                azel_file_1 << sample_offset_cnt << " " << std::fixed
                            << std::setprecision(1) << tracker.get_azimuth(0) << " "
                            << tracker.get_elevation(0) << " "
                            << int(tracker.get_range(0)) << std::endl;
            }
        }

        if (tracker.is_trackable(1) && sat2_downlink > 0) {
            float correction =
                (real_center_freq - sat2_downlink - tracker.get_doppler(1)) / sample_rate;
            recorder2.set_correction(correction);
            if (tracker.get_elevation(1) >= elevation_limit)
                dumper_correction = correction;

            if (!recorder2.is_running() && tracker.get_elevation(1) >= elevation_limit) {
                std::string basename = tracker.get_filename(1, device);
                if (log_az_el) {
                    azel_file_2.open(basename + ".plog");
                    start2 = std::chrono::steady_clock::now();
                }
                recorder2.start(basename.c_str());
                Json::Value root;
                root["options"] = options;
                tracker.add_json_data(1, root);
                write_json(root, basename);
            } else if (recorder2.is_running() &&
                       tracker.get_elevation(1) < elevation_limit) {
                recorder2.stop();
                azel_file_2.close();
            }

            if (recorder2.is_running() && (delay_count % 50 == 0) && log_az_el) {
                std::chrono::steady_clock::time_point now =
                    std::chrono::steady_clock::now();
                int sample_offset_cnt =
                    output_sample_rate / 1000 *
                    std::chrono::duration_cast<std::chrono::milliseconds>(now - start2)
                        .count();
                azel_file_2 << sample_offset_cnt << " " << std::fixed
                            << std::setprecision(1) << tracker.get_azimuth(1) << " "
                            << tracker.get_elevation(1) << " "
                            << int(tracker.get_range(1)) << std::endl;
            }
        }

        dumper.set_correction(dumper_correction);

        if (delay_count == 0) {
            std::array<float, 2> stat = dongle.get_statistics();
            unsigned int overruns = dongle.get_overruns();

            if (print_statistics) {
                std::stringstream msg;
                msg << "INFO: signal amplitude maximum: " << std::setprecision(5)
                    << std::fixed << stat[0] << ", average: " << stat[1]
                    << ", total overruns: " << overruns << std::endl;
                std::cerr << msg.str();
            } else {
                if (overruns != last_overruns) {
                    std::stringstream msg;
                    msg << "WARNING: total overruns: " << overruns << std::endl;
                    std::cerr << msg.str();
                    last_overruns = overruns;
                }
                if (stat[0] != last_max_signal) {
                    last_max_signal = stat[0];
                    last_max_signal_count = 0;
                } else if (++last_max_signal_count >= 10) {
                    std::stringstream msg;
                    msg << "WARNING: signal amplitude max is permanently: "
                        << std::setprecision(5) << std::fixed << stat[0] << ", ";
                    if (stat[0] <= 0.0f)
                        msg << "check the rtl dongle";
                    else if (stat[0] >= 1.0f)
                        msg << "reduce the gain";
                    else
                        msg << "check the antenna";
                    msg << std::endl;
                    std::cerr << msg.str();
                    last_max_signal_count = 0;
                }
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (do_exit)
        std::cerr << "INFO: aborted by signal" << std::endl;

    if (recorder1.is_running())
        recorder1.stop();

    if (recorder2.is_running())
        recorder2.stop();

    if (dumper.is_running())
        dumper.stop();

    dongle.stop();

    return 0;
}
