/*
 * Copyright 2019-2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef PREDICT_H
#define PREDICT_H

#include <json/json.h>
#include <string>

struct station_data {
    std::string callsign;
    double stnlat;
    double stnlong;
    int stnalt;
};

int read_data_files(const long* catnums, const char** names, struct station_data& sta);
void json_to_station_data(const Json::Value& station_root, struct station_data& sta);
void json_to_sat_data(const Json::Value& sat_root);
int find_sat_idx_by_name(const char* name);
int find_sat_idx_by_catnum(long catnum);
const char* sat_name_by_catnum(long catnum);
const char* sat_name_by_idx(int idx);
const char* sat_tle1_by_idx(int idx);
const char* sat_tle2_by_idx(int idx);
long sat_catnum_by_idx(int idx);
void PreCalc(int satellite_index);
void Calc(double daynum);
double calc_tle_age();

extern double sat_azi, sat_ele, sat_range_rate, sat_range;
extern long rv;

#endif
