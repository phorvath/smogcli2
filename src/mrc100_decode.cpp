/*
 * Copyright 2020-2023 Miklos Maroti, Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _WIN32
#include <unistd.h>
#else
#include "getopt/getopt.h"
#endif

#include "coherent.hpp"
#include "decoder.hpp"
#include "ra/ra_config.h"
#include "ra/ra_encoder.h"
#include "satellite.hpp"
#include "tle.hpp"
#include <json/json.h>
#include <algorithm>
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>
#include <random>

static std::atomic<bool> do_exit(false);

static void sighandler(int) { do_exit = true; }

std::map<std::string, int> pkt_coded_len_map = { { "ra126", (252 + 8) * 8 },
                                                 { "syncpkt", (252 + 8) * 8 } };

// 1 byte type, 2 bytes crc, 123 bytes payload
std::vector<uint8_t> sync4_data = {
    0xc5, 0x12, 0xb8, 0x88, 0x36, 0x31, 0xB1, 0xC8, 0x49, 0x62, 0x0C, 0xA4, 0xF4, 0x36,
    0x93, 0x09, 0xE4, 0x65, 0x88, 0x01, 0x8C, 0x57, 0x48, 0xD9, 0xC2, 0x44, 0x56, 0x7B,
    0x95, 0x20, 0xEB, 0x5D, 0x19, 0xB0, 0xFA, 0x02, 0x49, 0x0C, 0x7E, 0x4A, 0xD5, 0xDC,
    0x4D, 0x0C, 0x7E, 0xC5, 0x65, 0x43, 0x94, 0xD5, 0xE0, 0xE1, 0x59, 0x2A, 0x1B, 0x2F,
    0xF3, 0x2C, 0x4D, 0xD5, 0x02, 0x6F, 0xA7, 0x6E, 0x5B, 0x2B, 0xDD, 0x2F, 0x27, 0xDC,
    0x96, 0x65, 0x09, 0x00, 0xD9, 0x14, 0xA0, 0x3B, 0x16, 0x72, 0xD6, 0x3A, 0x62, 0x4D,
    0x17, 0x5F, 0xE6, 0xCC, 0x6E, 0x2B, 0x49, 0x4B, 0x5D, 0x39, 0xFD, 0x43, 0xDC, 0xBD,
    0xF4, 0x28, 0xDF, 0xB4, 0x89, 0x80, 0x85, 0xD3, 0x02, 0xC3, 0xAA, 0xE3, 0xEB, 0xBD,
    0x08, 0x75, 0x75, 0x8B, 0xAC, 0xB3, 0x4C, 0x8D, 0xCF, 0xC2, 0x98, 0x24, 0x5A, 0x52
}; // 126 bytes

void test_sync4()
{
    assert(sync4_data.size() == 126);

    ra_length_init(sync4_data.size() / 2);
    ra_encoder_init((const uint16_t*)(sync4_data.data()));

    std::cout << "data length: " << ra_data_length * 2
              << " bytes, code length: " << ra_code_length * 2 << " bytes" << std::endl;
    std::cout << "code bytes: " << std::setfill('0') << std::hex;

    for (size_t i = 0; i < ra_code_length; i++) {
        uint16_t word = ra_encoder_next();
        int low = word & 0xff;
        int high = (word >> 8) & 0xff;
        std::cout << "0x" << std::setw(2) << low << ", 0x" << std::setw(2) << high
                  << ", ";
    }
    std::cout << std::endl;
}

void test_detector()
{
    float freq = 0.0f;
    float noise = 0.0f;
    std::mt19937 rng(1);
    std::normal_distribution<float> dist(0.0f, std::sqrt(0.5f));

    std::vector<uint8_t> bytes = { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
                                   0x2d, 0xd4, 0x63, 0xc5, 0x35, 0x99, 0x01, 0x23,
                                   0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23,
                                   0x45, 0x67, 0x89, 0xab, 0xcd, 0xef };
    std::vector<std::complex<float>> input = gmsk_encode_bytes(bytes, 40);
    for (int i = 0; i < 0; i++)
        input.insert(input.begin(), input[0]);

    for (size_t i = 0; i < input.size(); i++) {
        input[i] *= std::complex<float>(std::cos(freq * i), std::sin(freq * i));
        input[i] += noise * std::complex<float>(dist(rng), dist(rng));
    }

    input[2560] = std::complex<float>(0.98777, -0.16666);

    // input_spb = 40, 8 bytes = 64 bits = 64 * 40 = 2560 samples
    // window = 2048 samples, stride 20 * 40 = 800
    preamble_detector detector(window_gaussian(2048, 0.4f),
                               800,
                               40,
                               5,
                               1024,
                               { 0x2d, 0xd4, 0x63, 0xc5, 0x35, 0x99 },
                               5.0f,
                               2.0f,
                               11);

    for (size_t i = 0; i + detector.get_stride() <= input.size();
         i += detector.get_stride()) {
        const std::vector<preamble_detector::packet_t>& packets =
            detector.work(input.data() + i);

        for (const preamble_detector::packet_t& packet : packets)
            detector.print_packet(packet);
    }
}

void test_viterbi()
{
    float freq = 0.0f;
    float noise = 0.0f;
    std::mt19937 rng(1);
    std::normal_distribution<float> dist(0.0f, std::sqrt(0.5f));

    std::vector<uint8_t> bytes = { 0x2d, 0xd4 };
    std::vector<std::complex<float>> samples = gmsk_encode_bytes(bytes, 5);

    for (size_t i = 0; i < samples.size(); i++) {
        samples[i] *= std::complex<float>(std::cos(freq * i), std::sin(freq * i));
        samples[i] += noise * std::complex<float>(dist(rng), dist(rng));
    }

    viterbi_demod demod(5);
    demod.demodulate(samples.data(), samples.size(), { 0x2d });
    demod.print_stats();
}

void usage(void)
{
    std::cerr << "MRC-100 demodulator and decoder\n\n"
                 "Usage: mrc100_decode [-options] [filenames]\n"
                 "\t-T runs internal test\n"
                 "\t-b bits per second (default: 1250, 2500, 5000 and 12500 bps)\n"
                 "\t-r sampling rate of input (default: 50000 sps)\n"
                 "\t-t tone detection sensitivity in dB (default: 2 dB)\n"
                 "\t-s sync detection sensitivity in dB (default: 1 dB)\n"
                 "\t-d data detection sensitivity in dB (default: 0 dB)\n"
                 "\t-C disable CRC checking (default: off)\n"
                 "\t-D disable Viterbi demodulator (default: off)\n"
                 "\t-p write packet files instead of stdout (default: off)\n"
                 "\t-S print raw samples for each packet (default: off)\n"
                 "\t-Y decode only sync packets (default: off)\n"
                 "\t-l limit the search to freq offset (default: 25000 Hz)\n"
                 "\t-A use alternative CRC checking for S-band (default: False)\n"
                 "\t-h prints this help message\n";
}

bool read_json(Json::Value& root, const std::string& metafilename)
{
    std::ifstream metafile;
    metafile.open(metafilename, std::ifstream::binary);

    if (metafile) {
        Json::CharReaderBuilder rbuilder;
        std::string errs;
        if (!Json::parseFromStream(rbuilder, metafile, &root, &errs)) {
            std::cerr << "WARNING: Failed to parse the metafile " << metafilename
                      << std::endl
                      << errs << std::endl;
            return false;
        } else {
            std::cerr << "INFO: using metafile " << metafilename << std::endl;
        }
        metafile.close();
        return true;
    } else {
        std::cerr << "WARNING: Failed to open the metafile " << metafilename << std::endl;
    }
    return false;
}

double calc_packet_duration(const soft_decoder::packet_t& packet, int data_rate)
{
    return (pkt_coded_len_map.find(packet.type)->second) / float(data_rate);
}

bool create_preamble_detector(size_t samp_rate,
                              size_t input_bps,
                              float freq_limit,
                              const std::vector<uint8_t>& sync_bytes,
                              float tone_snr_db,
                              float sync_snr_db,
                              size_t output_bytes,
                              std::vector<preamble_detector>& detectors)
{
    if (samp_rate % input_bps != 0)
        return false;
    size_t input_spb = samp_rate / input_bps;

    size_t output_spb;
    if (input_spb % 5 == 0)
        output_spb = 5;
    else if (input_spb % 4 == 0)
        output_spb = 4;
    else
        return false;

    size_t window_size;
    size_t stride_size;
    if (input_spb < 8) { // 4
        window_size = 256;
        stride_size = 100;
    } else if (input_spb < 16) { // 10
        window_size = 512;
        stride_size = 200;
    } else if (input_spb < 32) { // 20
        window_size = 1024;
        stride_size = 400;
    } else if (input_spb < 64) { // 40, 50
        window_size = 2048;
        stride_size = 800;
    } else
        return false;

    if (!signal_downsampler::is_supported(input_spb, output_spb))
        return false;

    size_t max_freq_bin = std::ceil(freq_limit * window_size / samp_rate);

    detectors.emplace_back(window_gaussian(window_size, 0.4f),
                           stride_size,
                           input_spb,
                           output_spb,
                           max_freq_bin,
                           sync_bytes,
                           tone_snr_db,
                           sync_snr_db,
                           output_bytes);
    return true;
}

int main(int argc, char* argv[])
{
    std::vector<size_t> input_bps;
    size_t samp_rate = 50000;
    float tone_sensitivity = 2.0f;
    float sync_sensitivity = 1.0f;
    float data_sensitivity = 0.0f;
    bool write_packets = false;
    bool disable_crc_check = false;
    bool enable_viterbi = true;
    bool print_samples = false;
    bool decode_sync_only = false;
    bool calc_sat_pos = true;
    bool sband_crc_check = false;
    double start_daynum = 0.0;
    float freq_limit = 25000;

    int opt;
    while ((opt = getopt(argc, argv, "b:r:t:s:d:l:pCDTSYAh")) != -1) {
        switch (opt) {
        case 'b':
            input_bps.push_back(static_cast<size_t>(std::atol(optarg)));
            break;
        case 'r':
            samp_rate = static_cast<size_t>(std::atol(optarg));
            break;
        case 't':
            tone_sensitivity = std::atof(optarg);
            break;
        case 's':
            sync_sensitivity = std::atof(optarg);
            break;
        case 'd':
            data_sensitivity = std::atof(optarg);
            break;
        case 'l':
            freq_limit = std::atof(optarg);
            break;
        case 'p':
            write_packets = true;
            break;
        case 'C':
            disable_crc_check = true;
            break;
        case 'D':
            enable_viterbi = false;
            break;
        case 'S':
            print_samples = true;
            break;
        case 'Y':
            decode_sync_only = true;
            break;
        case 'A':
            sband_crc_check = true;
            break;
        case 'T':
            // test_detector();
            // test_viterbi();
            test_sync4();
            return 0;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    if (input_bps.empty())
        input_bps = { 1250, 2500, 5000, 12500 };

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    while (!do_exit && optind < argc) {
        std::string filename = argv[optind++];

        size_t pos = filename.find_last_of('.');
        if (pos == std::string::npos)
            pos = filename.size();

        const std::string basename = filename.substr(0, pos);
        std::string filename2 = basename + ".pkts";

        std::cerr << "INFO: reading " << filename;
        if (write_packets)
            std::cerr << ", writing " << filename2;
        std::cerr << std::endl;

        if (freq_limit != 25000)
            std::cerr << "INFO: limiting the search range to " << freq_limit / 1000.
                      << " kHz." << std::endl;

        Json::Value meta_root;
        if (!read_json(meta_root, basename + ".meta")) {
            calc_sat_pos = false;
        } else {
            Json::Value tle_root = meta_root["tle"];
            start_daynum = tle_root.get("daynum", 0.0).asDouble();
            if (start_daynum == 0.0) {
                calc_sat_pos = false;
            }
        }

        file_source source;
        source.open(filename.c_str(), file_source::AUTO);
        if (!source.is_open())
            continue;

        std::ofstream cout;
        if (write_packets) {
            cout.open(filename2.c_str(), std::ofstream::trunc);
        }

        viterbi_demod demod4(4);
        viterbi_demod demod5(5);

        sat_tracker tracker(meta_root);

        int decd_ra126_cnt = 0;

        int decd_1250_cnt = 0;
        int decd_2500_cnt = 0;
        int decd_5000_cnt = 0;
        int decd_12500_cnt = 0;
        int decd_25000_cnt = 0;

        int sync_ra126_cnt = 0;

        std::vector<preamble_detector> detectors;

        for (size_t bps : input_bps) {
            if (!create_preamble_detector(
                    samp_rate,
                    bps,
                    freq_limit,
                    std::vector<uint8_t>({ 0xe3, 0x1c, 0x9d, 0xae }),
                    tone_sensitivity,
                    sync_sensitivity,
                    4 + 252 + 6, // not sure about the +6 part
                    detectors)) {
                std::stringstream msg;
                msg << "WARNING: unsupported sampling rate " << samp_rate << " and "
                    << bps << " bsp combination!" << std::endl;
                std::cerr << msg.str();
                continue;
            }
        }

        if (detectors.empty()) {
            std::cerr << "WARNING: no detector is available to handle the data packets!"
                      << std::endl;
        }

        std::vector<std::complex<float>> input(800);
        soft_decoder decoder(sband_crc_check ? CRC_MRC100_SBAND : CRC_MRC100);

        size_t length = 0;
        auto start = std::chrono::high_resolution_clock::now();

        while (!do_exit) {
            if (source.read_float(input) != 0)
                break;

            for (preamble_detector& detector : detectors) {
                assert(samp_rate % detector.get_input_spb() == 0);
                assert(input.size() % detector.get_stride() == 0);

                size_t spb = detector.get_input_spb();

                for (size_t i = 0; i < input.size(); i += detector.get_stride()) {
                    const std::vector<preamble_detector::packet_t>& packets =
                        detector.work(input.data() + i);

                    for (const preamble_detector::packet_t& packet1 : packets) {
                        size_t header_bits;

                        std::vector<float> bits;
                        if (enable_viterbi && detector.get_output_spb() == 4) {
                            bits = demod4.demodulate(packet1.samples.data(),
                                                     packet1.samples.size(),
                                                     detector.get_sync_bytes());
                            header_bits = 0;
                        } else if (enable_viterbi && detector.get_output_spb() == 5) {
                            bits = demod5.demodulate(packet1.samples.data(),
                                                     packet1.samples.size(),
                                                     detector.get_sync_bytes());
                            header_bits = 0;
                        } else {
                            bits = gmsk_soft_decode2(packet1.samples,
                                                     detector.get_output_spb());
                            header_bits = detector.get_sync_size() * 8;
                        }

                        soft_decoder::packet_t packet2 =
                            decoder.work_mrc100(bits.data() + header_bits,
                                                bits.size() - header_bits,
                                                decode_sync_only);

                        // detected sync again through RA
                        if (packet2.data == sync4_data) {
                            packet2.type = "syncpkt";
                            packet2.data = { 0xfd, 0x04 };
                        }

                        // sync packets have 2-byte payloads
                        if (packet2.snr >= data_sensitivity &&
                            (packet2.crc || disable_crc_check ||
                             packet2.data.size() == 2)) {
                            std::stringstream msg;
                            detector.print_packet(samp_rate, packet1, msg);
                            msg << ", ";
                            decoder.print_packet(packet2, msg);
                            if (calc_sat_pos) {
                                double begin_daynum =
                                    packet1.position / 86400.0 / double(samp_rate) +
                                    start_daynum;
                                double duration =
                                    calc_packet_duration(packet2, int(samp_rate / spb));
                                msg << ", ";
                                tracker.print_packet(begin_daynum, duration / 86400, msg);
                            }
                            if (print_samples) {
                                msg << ", ";
                                detector.print_samples(packet1, msg);
                            }
                            msg << std::endl;
                            (write_packets ? cout : std::cout) << msg.str();

                            if (std::strcmp(packet2.type, "ra126") == 0)
                                ++decd_ra126_cnt;

                            if (std::strcmp(packet2.type, "syncpkt") != 0) {
                                switch (size_t(samp_rate / spb)) {
                                case 1250:
                                    ++decd_1250_cnt;
                                    break;
                                case 2500:
                                    ++decd_2500_cnt;
                                    break;
                                case 5000:
                                    ++decd_5000_cnt;
                                    break;
                                case 12500:
                                    ++decd_12500_cnt;
                                    break;
                                case 25000:
                                    ++decd_25000_cnt;
                                    break;
                                }
                            }

                            if (std::strcmp(packet2.type, "syncpkt") == 0) {
                                ++sync_ra126_cnt;
                            }
                        }
                    }
                }
            }

            length += input.size();
        }

        double elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(
                             std::chrono::high_resolution_clock::now() - start)
                             .count();

        source.close();
        if (cout.is_open())
            cout.close();

        size_t tone_detected = 0;
        size_t sync_detected = 0;
        for (preamble_detector& detector : detectors) {
            tone_detected += detector.get_tone_detected();
            sync_detected += detector.get_sync_detected();
        }

        std::cerr << "INFO: finished " << filename << ", samples: " << length
                  << ", speed: " << (length / elapsed * 1e-6)
                  << " msps, tone detected: " << tone_detected
                  << " pkts, sync detected: " << sync_detected << " pkts" << std::endl;

        {
            std::stringstream msg;
            msg << "INFO: decoded data packets: " << decd_ra126_cnt;
            if (decd_ra126_cnt)
                msg << ", RA126: " << decd_ra126_cnt;
            if (decd_1250_cnt)
                msg << ", 1250 bps: " << decd_1250_cnt;
            if (decd_2500_cnt)
                msg << ", 2500 bps: " << decd_2500_cnt;
            if (decd_5000_cnt)
                msg << ", 5000 bps: " << decd_5000_cnt;
            if (decd_12500_cnt)
                msg << ", 12500 bps: " << decd_12500_cnt;
            if (decd_25000_cnt)
                msg << ", 25000 bps: " << decd_25000_cnt;
            msg << std::endl;
            std::cerr << msg.str();
        }

        {
            std::stringstream msg;
            msg << "INFO: decoded sync packets: " << sync_ra126_cnt << std::endl;
            std::cerr << msg.str();
        }

        if (optind < argc)
            std::cerr << std::endl;
    }

    if (do_exit)
        std::cerr << "INFO: aborted by signal" << std::endl;

    return 0;
}
