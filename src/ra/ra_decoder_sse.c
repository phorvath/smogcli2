/* Copyright (C) Miklos Maroti 2015 */

#ifdef __SSE4_1__

#include "ra_decoder_sse.h"
#include "ra_lfsr.h"
#include <assert.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include <stdbool.h>
#include <tmmintrin.h>

/* --- REPEAT ACCUMULATE SSE 4.1 DECODER --- */

__m128i ra_dataword_sse[RA_MAX_DATA_LENGTH];
__m128i ra_codeword_sse[RA_MAX_CODE_LENGTH];
__m128i ra_forward_sse[RA_MAX_DATA_LENGTH];

void ra_prepare_sse(const float* softbits, float scaling)
{
    int index;
    __m128 scale, data1, data2, data3, data4;
    __m128i zero, res1, res2, res3, res4;

    zero = _mm_set1_epi8(0);
    for (index = 0; index < ra_data_length; index++)
        _mm_store_si128(ra_dataword_sse + index, zero);

    scale = _mm_set1_ps(scaling);
    for (index = 0; index < ra_code_length; index++) {
        data1 = _mm_loadu_ps(softbits + index * 16);
        data2 = _mm_loadu_ps(softbits + index * 16 + 4);
        data3 = _mm_loadu_ps(softbits + index * 16 + 8);
        data4 = _mm_loadu_ps(softbits + index * 16 + 12);

        data1 = _mm_mul_ps(data1, scale);
        data2 = _mm_mul_ps(data2, scale);
        data3 = _mm_mul_ps(data3, scale);
        data4 = _mm_mul_ps(data4, scale);

        res1 = _mm_cvtps_epi32(data1);
        res2 = _mm_cvtps_epi32(data2);
        res3 = _mm_cvtps_epi32(data3);
        res4 = _mm_cvtps_epi32(data4);

        res1 = _mm_packs_epi32(res1, res2);
        res3 = _mm_packs_epi32(res3, res4);
        res1 = _mm_packs_epi16(res1, res3);

        _mm_store_si128(ra_codeword_sse + index, res1);
    }
}

#define SSE_LLR_MIN(A, B)                                                         \
    {                                                                             \
        xsig = _mm_xor_si128(A, B);                                               \
        A = _mm_min_epu8(_mm_abs_epi8(A), _mm_abs_epi8(B));                       \
        A = _mm_blendv_epi8(_mm_min_epu8(A, c127), _mm_subs_epi8(zero, A), xsig); \
    }

void ra_improve_sse(__m128i* codeword, int puncture, bool half)
{
    int index, pos = 0;
    __m128i zero, c127, c128, accu, data, code, left, xsig;

    assert(ra_data_length > 0); /* to avoid pos uninitialized warning */

    zero = _mm_set1_epi8(0);
    c127 = _mm_set1_epi8(127);
    c128 = _mm_set1_epi8(-128);

    accu = c127;
    for (index = 0; index < ra_data_length; index++) {
        pos = ra_lfsr_next();
        data = _mm_load_si128(ra_dataword_sse + pos);
        _mm_store_si128(ra_forward_sse + index, accu);

        SSE_LLR_MIN(accu, data);

        if ((index + 1) % puncture == 0) {
            code = _mm_load_si128(codeword++);
            accu = _mm_adds_epi8(accu, code);
        }

        accu = _mm_alignr_epi8(accu, accu, 1);
    }

    if (ra_data_length % puncture != 0) {
        code = _mm_load_si128(codeword);
        code = _mm_alignr_epi8(code, code, 1);
        accu = _mm_adds_epi8(accu, code);
        accu = _mm_adds_epi8(accu, code);
    }

    for (index = ra_data_length - 1; index >= 0; index--) {
        left = _mm_load_si128(ra_forward_sse + index);
        data = _mm_load_si128(ra_dataword_sse + pos);

        accu = _mm_alignr_epi8(accu, accu, 15);

        if ((index + 1) % puncture == 0) {
            code = _mm_load_si128(--codeword);
            accu = _mm_adds_epi8(accu, code);
        }

        SSE_LLR_MIN(left, accu);
        SSE_LLR_MIN(accu, data);

        if (half) {
            data = _mm_add_epi8(data, c128);
            data = _mm_avg_epu8(data, c128);
            data = _mm_sub_epi8(data, c128);
        }

        left = _mm_adds_epi8(left, data);
        _mm_store_si128(ra_dataword_sse + pos, left);
        pos = ra_lfsr_prev();
    }
}

void ra_decide_sse(uint16_t* packet)
{
    int index;

    for (index = 0; index < ra_data_length; index++)
        packet[index] = _mm_movemask_epi8(_mm_load_si128(ra_dataword_sse + index));
}

void ra_decoder_sse(const float* softbits, uint16_t* packet, int passes, float scaling)
{
    int count, seqno;
    __m128i* codeword;

    ra_prepare_sse(softbits, 24.0f * scaling);

    for (count = 0; count < passes; count++) {
        codeword = ra_codeword_sse;

        for (seqno = 0; seqno < 4; seqno++) {
            ra_lfsr_init(seqno);
            ra_improve_sse(codeword, seqno == 0 ? 1 : RA_PUNCTURE_RATE, count > 0);
            codeword += seqno == 0 ? ra_data_length : ra_chck_length;
        }

        assert(ra_codeword_sse + ra_code_length == codeword);
    }

    ra_decide_sse(packet);
}

#endif
