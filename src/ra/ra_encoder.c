/* Copyright (C) Miklos Maroti 2015-2023 */

#include "ra_encoder.h"
#include "ra_lfsr.h"

/* --- REPEAT ACCUMULATE ENCODER: rate 1/4, punctured, twisted parallel --- */

const ra_word_t* ra_packet;
ra_word_t ra_nextword;
uint8_t ra_passno;

void ra_encoder_init(const ra_word_t* packet)
{
    ra_packet = packet;
    ra_nextword = 0;
    ra_passno = 0;
    ra_lfsr_init(0);
}

ra_word_t ra_encoder_next(void)
{
    ra_index_t pos;
    ra_word_t word;
    uint8_t count;

    word = ra_nextword;

    count = ra_passno == 0 ? 1 : RA_PUNCTURE_RATE;
    do {
        word = (word >> 1) | (word << RA_BITSHIFT);
        pos = ra_lfsr_next();
        word ^= ra_packet[pos];
    } while (pos != ra_passno && --count != 0);

    if (count != 0) {
        ra_nextword = 0;
        ra_passno = (ra_passno + 1) % 4;
        ra_lfsr_init(ra_passno);
    } else
        ra_nextword = word;

    return word;
}

void ra_encoder(const ra_word_t* packet, ra_word_t* output)
{
    ra_encoder_init(packet);
    for (ra_index_t i = 0; i < ra_code_length; i++)
        *(output++) = ra_encoder_next();
}
