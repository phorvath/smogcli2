/* Copyright (C) Miklos Maroti 2015-2023 */

#ifndef __RA_LFSR_H__
#define __RA_LFSR_H__

#include "ra_config.h"

#ifdef __cplusplus
extern "C" {
#endif

void ra_lfsr_init(uint8_t seqno);
ra_index_t ra_lfsr_next(void);
ra_index_t ra_lfsr_prev(void);

#ifdef __cplusplus
}
#endif

#endif //__RA_LFSR_H__
