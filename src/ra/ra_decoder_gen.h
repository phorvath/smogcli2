/* Copyright (C) Miklos Maroti 2015-2016 */

#ifndef __RA_DECODER_GEN_H__
#define __RA_DECODER_GEN_H__

#include "ra_config.h"

#ifdef __cplusplus
extern "C" {
#endif

void ra_decoder_gen(const float* softbits, ra_word_t* packet, int passes);

#ifdef __cplusplus
}
#endif

#endif //__RA_DECODER_GEN_H__
