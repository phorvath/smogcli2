/*
 * Copyright 2019-2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "satellite.hpp"
#include <iostream>

std::vector<float> sat_downconv_32::filter1_taps = {
    -0.031555700171, 0.000000000000, 0.281553722827,  0.500000000000,
    0.281553722827,  0.000000000000, -0.031555700171, 0.000000000000,
    0.000000000000,  0.000000000000, 0.000000000000,  0.000000000000
};

std::vector<float> sat_downconv_32::filter2_taps = {
    0.006252968323,  0.000000000000, -0.049978313897, 0.000000000000,
    0.293725694512,  0.500000000000, 0.293725694512,  0.000000000000,
    -0.049978313897, 0.000000000000, 0.006252968323,  0.000000000000
};

std::vector<float> sat_downconv_32::filter3_taps = {
    0.007618542238,  0.000000000000, -0.053559407732, 0.000000000000,
    0.295965078427,  0.500000000000, 0.295965078427,  0.000000000000,
    -0.053559407732, 0.000000000000, 0.007618542238,  0.000000000000
};

std::vector<float> sat_downconv_32::filter4_taps = {
    -0.000655211223, 0.000000000000, 0.003706523698, 0.000000000000,  -0.012693634822,
    0.000000000000,  0.034098213327, 0.000000000000, -0.085217367191, 0.000000000000,
    0.310739440848,  0.500000000000, 0.310739440848, 0.000000000000,  -0.085217367191,
    0.000000000000,  0.034098213327, 0.000000000000, -0.012693634822, 0.000000000000,
    0.003706523698,  0.000000000000, -0.000655211223
};

std::vector<float> sat_downconv_32::filter5_taps = {
    0.000145409563,  0.000000000000,  -0.000344067610, 0.000000000000,  0.000725745648,
    0.000000000000,  -0.001355951435, 0.000000000000,  0.002332276022,  0.000000000000,
    -0.003775358227, 0.000000000000,  0.005835644499,  0.000000000000,  -0.008709639755,
    0.000000000000,  0.012674816594,  0.000000000000,  -0.018169014590, 0.000000000000,
    0.025983135855,  0.000000000000,  -0.037791065181, 0.000000000000,  0.057958795489,
    0.000000000000,  -0.102591577476, 0.000000000000,  0.317124009005,  0.500000000000,
    0.317124009005,  0.000000000000,  -0.102591577476, 0.000000000000,  0.057958795489,
    0.000000000000,  -0.037791065181, 0.000000000000,  0.025983135855,  0.000000000000,
    -0.018169014590, 0.000000000000,  0.012674816594,  0.000000000000,  -0.008709639755,
    0.000000000000,  0.005835644499,  0.000000000000,  -0.003775358227, 0.000000000000,
    0.002332276022,  0.000000000000,  -0.001355951435, 0.000000000000,  0.000725745648,
    0.000000000000,  -0.000344067610, 0.000000000000,  0.000145409563
};

sat_downconv_32::sat_downconv_32(size_t work_length)
    : buffer2(32 * work_length),
      sinusoid(0.0f),
      buffer3(32 * work_length),
      buffer4(32 * work_length),
      filter1(filter1_taps),
      buffer5(16 * work_length),
      filter2(filter2_taps),
      buffer6(8 * work_length),
      filter3(filter3_taps),
      buffer7(4 * work_length),
      filter4(filter4_taps, 2),
      buffer8(2 * work_length),
      filter5(filter5_taps, 2),
      buffer9(work_length)
{
}

sat_downconv_32::~sat_downconv_32() {}

void sat_downconv_32::work()
{
    sinusoid.work(buffer3);
    multiply.work(buffer2, buffer3, buffer4);
    filter1.work(buffer4, buffer5);
    filter2.work(buffer5, buffer6);
    filter3.work(buffer6, buffer7);
    filter4.work(buffer7, buffer8);
    filter5.work(buffer8, buffer9);
}

std::vector<float> sat_downconv_8::filter1_taps = {
    0.008157043726,  0.000000000000, -0.054825932571, 0.000000000000,
    0.296718218219,  0.500000000000, 0.296718218219,  0.000000000000,
    -0.054825932571, 0.000000000000, 0.008157043726
};

std::vector<float> sat_downconv_8::filter2_taps = {
    -0.001255802632, 0.000000000000, 0.005428678796, 0.000000000000,  -0.015751048176,
    0.000000000000,  0.037896256931, 0.000000000000, -0.088436905907, 0.000000000000,
    0.312007527785,  0.500000000000, 0.312007527785, 0.000000000000,  -0.088436905907,
    0.000000000000,  0.037896256931, 0.000000000000, -0.015751048176, 0.000000000000,
    0.005428678796,  0.000000000000, -0.001255802632
};

std::vector<float> sat_downconv_8::filter3_taps = {
    0.000923126981,  0.000000000000,  -0.003505636193, 0.000000000000, 0.009432373431,
    0.000000000000,  -0.021161362876, 0.000000000000,  0.043580679335, 0.000000000000,
    -0.092799111824, 0.000000000000,  0.313647003137,  0.500000000000, 0.313647003137,
    0.000000000000,  -0.092799111824, 0.000000000000,  0.043580679335, 0.000000000000,
    -0.021161362876, 0.000000000000,  0.009432373431,  0.000000000000, -0.003505636193,
    0.000000000000,  0.000923126981
};

sat_downconv_8::sat_downconv_8(size_t work_length)
    : buffer2(32 * work_length),
      sinusoid(0.0f),
      buffer3(32 * work_length),
      buffer4(32 * work_length),
      filter1(filter1_taps),
      buffer5(16 * work_length),
      filter2(filter2_taps, 2),
      buffer6(8 * work_length),
      filter3(filter3_taps, 2),
      buffer7(4 * work_length)
{
}

sat_downconv_8::~sat_downconv_8() {}

void sat_downconv_8::work()
{
    sinusoid.work(buffer3);
    multiply.work(buffer2, buffer3, buffer4);
    filter1.work(buffer4, buffer5);
    filter2.work(buffer5, buffer6);
    filter3.work(buffer6, buffer7);
}

sat_recorder::sat_recorder(buffer_t& buffer,
                           size_t reader,
                           uint32_t input_sample_rate,
                           uint32_t output_sample_rate,
                           size_t work_length,
                           size_t file_buffer_length)
    : buffer(buffer),
      reader(reader),
      decimation(input_sample_rate / output_sample_rate),
      ddc_32(decimation == 32 ? work_length : 0),
      ddc_8(decimation == 8 ? work_length : 0),
      sink(file_sink::CF32, output_sample_rate, file_buffer_length),
      running(false)
{
    assert(input_sample_rate % output_sample_rate == 0);
    assert(decimation == 32 || decimation == 8);

    std::stringstream msg;
    msg << "INFO: ***** DDC input " << input_sample_rate << " sps, output "
        << output_sample_rate << " sps" << std::endl;
    std::cerr << msg.str();

    buffer.enable(reader, false);
}

void sat_recorder::worker()
{
    std::string filename1;
    std::string filename2;
    if (basename == "-") {
        filename1 = "STDOUT";
        filename2 = "-";
    } else {
        filename1 = basename + ".cf32";
        filename2 = filename1;
    }

    {
        std::stringstream msg;
        msg << "INFO: ***** started recording to " << filename1 << std::endl;
        std::cerr << msg.str();
    }

    sink.open(filename2.c_str());
    buffer.enable(reader, true);

    while (running) {
        if (decimation == 32) {
            if (buffer.read(reader, ddc_32.input()) != 0)
                break;

            ddc_32.set_correction(correction);
            ddc_32.work();
            sink.write(ddc_32.output());
        } else if (decimation == 8) {
            if (buffer.read(reader, ddc_8.input()) != 0)
                break;

            ddc_8.set_correction(correction);
            ddc_8.work();
            sink.write(ddc_8.output());
        }
    }

    buffer.enable(reader, false);
    sink.close();

    {
        std::stringstream msg;
        msg << "INFO: ***** finished recording to " << filename1 << std::endl;
        std::cerr << msg.str();
    }
}

void sat_recorder::start(const char* basename)
{
    this->basename = basename;
    assert(!running);
    running = true;
    thread = std::thread(&sat_recorder::worker, this);
}

void sat_recorder::stop()
{
    assert(running);
    running = false;
    buffer.enable(reader, false);
    thread.join();
}

samples_dumper::samples_dumper(buffer_t& buffer,
                               size_t reader,
                               uint32_t input_sample_rate,
                               uint32_t output_sample_rate,
                               size_t work_length,
                               size_t file_buffer_length,
                               bool enable_stdout,
                               bool enable_zmq)
    : buffer(buffer),
      reader(reader),
      decimation(input_sample_rate / output_sample_rate),
      ddc_32(decimation == 32 ? work_length : 0),
      ddc_8(decimation == 8 ? work_length : 0),
      enable_stdout(enable_stdout),
      file_sink(file_sink::CF32, output_sample_rate, file_buffer_length),
      enable_zmq(enable_zmq),
      zmq_sink(file_buffer_length),
      running(false)
{
    assert(input_sample_rate % output_sample_rate == 0);
    assert(decimation == 32 || decimation == 8);

    std::stringstream msg;
    msg << "INFO: Dumper DDC input " << input_sample_rate << " sps, output "
        << output_sample_rate << " sps" << std::endl;
    std::cerr << msg.str();

    buffer.enable(reader, false);
}

void samples_dumper::worker()
{
    if (enable_stdout) {
        std::cerr << "INFO: ***** start dumping to stdout\n";
        file_sink.open("-");
    }

    if (enable_zmq) {
        std::cerr << "INFO: ***** start dumping to ZMQ tcp://0.0.0.0:7207\n";
        zmq_sink.open();
    }

    buffer.enable(reader, true);

    while (running) {
        if (decimation == 32) {
            if (buffer.read(reader, ddc_32.input()) != 0)
                break;

            ddc_32.set_correction(correction);
            ddc_32.work();

            if (enable_stdout)
                file_sink.write(ddc_32.output());
            if (enable_zmq) {
                zmq_sink.write(ddc_32.output());
            }
        } else if (decimation == 8) {
            if (buffer.read(reader, ddc_8.input()) != 0)
                break;

            ddc_8.set_correction(correction);
            ddc_8.work();

            if (enable_stdout)
                file_sink.write(ddc_8.output());
            if (enable_zmq) {
                zmq_sink.write(ddc_8.output());
            }
        }
    }

    buffer.enable(reader, false);

    if (enable_stdout) {
        std::cerr << "INFO: ***** stop dumping to stdout\n";
        file_sink.close();
    }

    if (enable_zmq) {
        std::cerr << "INFO: ***** stop dumping to ZMQ tcp://0.0.0.0:7207\n";
        zmq_sink.close();
    }
}

void samples_dumper::start()
{
    assert(!running);
    if (enable_stdout || enable_zmq) {
        running = true;
        thread = std::thread(&samples_dumper::worker, this);
    }
}

void samples_dumper::stop()
{
    assert(running);
    running = false;
    buffer.enable(reader, false);
    thread.join();
}
