/*
 * Copyright 2020-2021 Peter Horvath, Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "ad936x_device.hpp"
#include <iostream>

#define MHZ(x) ((long long)(x * 1000000.0 + .5))
#define GHZ(x) ((long long)(x * 1000000000.0 + .5))

char ad936x_device::tmpstr[64];

ad936x_device::ad936x_device(buffer_t& buffer,
                             const std::string& context_uri,
                             size_t rx_buffer_size,
                             float lo_freq,
                             rx_gain_mode agc_mode,
                             int hw_gain)
    : lo_freq(lo_freq),
      agc_mode(agc_mode),
      hw_gain(hw_gain),
      uri(context_uri),
      rx_cfg(),
      tx_cfg(),
      rx_buffer_size(rx_buffer_size),
      do_exit(false),
      buffer(buffer),
      temp(convert_buffer_size, 0.0f),
      init_done(false)
{
    ctx = nullptr;
    tx = nullptr;
    rx = nullptr;
    rx0_i = nullptr;
    rx0_q = nullptr;
    tx0_i = nullptr;
    tx0_q = nullptr;
    rxbuf = nullptr;
    phy = nullptr;

    rssi_buf[0] = 0;
    hwgain_buf[0] = 0;

    assert(convert_buffer_size >= rx_buffer_size);
    print_libiio_info();

    // dbgfile.open("dbg.cf32");
}

ad936x_device::~ad936x_device()
{
    // dbgfile.close();
    destroy();
}

void ad936x_device::start()
{
    assert(init_done);
    assert(phy != nullptr);

    std::stringstream msg;
    msg << "INFO: starting PlutoSDR thread with " << buffer.size() << " bytes of buffer"
        << std::endl;
    std::cerr << msg.str();

    input_thread = std::thread(&ad936x_device::worker, this);
}

void ad936x_device::stop()
{
    std::stringstream msg;
    std::cerr << "INFO: stopping PlutoSDR recording thread..." << std::endl;
    std::cerr << msg.str();

    do_exit = true;
    input_thread.join();
    iio_buffer_cancel(rxbuf);
    buffer.clear();
}

void ad936x_device::worker()
{
    int stat_update_cnt = 0;
    iio_channel* chn = 0;
    get_phy_chan(RX, 0, &chn);

    while (!do_exit) {
        ssize_t nbytes_rx = iio_buffer_refill(rxbuf);
        if (nbytes_rx < 0) {
            std::cerr << "ERROR: Error refilling the receive buffer" << std::endl;
            // shutdown();
            continue;
        }

        assert(size_t(nbytes_rx) / 2 <= temp.size());
        convert.work(reinterpret_cast<int16_t*>(iio_buffer_first(rxbuf, rx0_i)),
                     temp.data(),
                     size_t(nbytes_rx) / 2);
        assert(size_t(nbytes_rx) * 2 <= buffer.size());
        buffer.write(temp.data(), 4 * temp.size(), true);

        if (stat_update_cnt++ >= 10) {
            stat_update_cnt = 0;
            std::lock_guard<std::mutex> lock(mutex);
            iio_channel_attr_read(chn, "hardwaregain", hwgain_buf, hwgain_buf_len);
            iio_channel_attr_read(chn, "rssi", rssi_buf, rssi_buf_len);
        }
    }
}

bool ad936x_device::init()
{
    const int err_str_len = 200;
    char err_str[err_str_len];

    if (uri == "") {
        ctx = autodetect_context(true);
        if (ctx == nullptr) {
            std::cerr << "ERROR: iio_create_default_context() failed." << std::endl;
            return false;
        }
    } else {
        ctx = iio_create_context_from_uri(uri.c_str());
        if (ctx == nullptr) {
            std::cerr << "ERROR: iio_create_context_from_uri() failed." << std::endl;
            return false;
        }
    }

    if (iio_context_get_devices_count(ctx) == 0) {
        std::cerr << "ERROR: no PlutoSDR devices found." << std::endl;
        return false;
    }

    std::cerr << "INFO: Acquiring AD9361 streaming devices..." << std::endl;
    if (!get_ad9361_stream_dev(ctx, RX, &rx)) {
        std::cerr << "ad936x_device: no RX device found" << std::endl;
        return false;
    }
    if (!get_ad9361_stream_dev(ctx, TX, &tx)) {
        std::cerr << "ad936x_device: no TX device found" << std::endl;
        return false;
    }

    phy = get_ad9361_phy(ctx);
    std::cerr << "INFO: Configuring AD9361 for TDD mode..." << std::endl;
    int ret = iio_device_debug_attr_write_bool(
        phy, "adi,frequency-division-duplex-mode-enable", 0);
    if (ret < 0) {
        iio_strerror(-ret, err_str, err_str_len);
        std::cerr << "ERROR: ad936x TDD activation failed: " << err_str << std::endl;
        return false;
    }
    ret = iio_device_debug_attr_write_bool(phy, "initialize", 1);
    if (ret < 0) {
        iio_strerror(-ret, err_str, err_str_len);
        std::cerr << "ERROR: ad936x TDD re-initialization failed: " << err_str
                  << std::endl;
        return false;
    }
    iio_device_attr_read(phy, "ensm_mode_available", err_str, err_str_len);
    std::cerr << "INFO: ad936x_device: ensm_mode_available: " << err_str << std::endl;
    iio_device_attr_write(phy, "ensm_mode", "rx");

    ret = iio_device_attr_write_raw(
        phy, "filter_fir_config", smogcli_16_ftr, smogcli_16_ftr_len);
    if (ret < 0) {
        iio_strerror(-ret, err_str, err_str_len);
        std::cerr << "ERROR: ad936x FIR config failed" << err_str << std::endl;
        return false;
    }
    ret = iio_channel_attr_write_bool(
        iio_device_find_channel(phy, "out", false), "voltage_filter_fir_en", true);
    if (ret < 0) {
        iio_strerror(-ret, err_str, err_str_len);
        std::cerr << "ERROR: ad936x FIR activation failed: " << err_str << std::endl;
        return false;
    }

    iio_device_attr_read(phy, "calib_mode", err_str, err_str_len);
    std::cerr << "INFO: ad936x_device: calib_mode: " << err_str << std::endl;
    iio_device_debug_attr_write_bool(phy, "bb_dc_offset_tracking_en", 1);
    iio_device_debug_attr_write_bool(phy, "rf_dc_offset_tracking_en", 1);
    iio_device_attr_read(phy, "bb_dc_offset_tracking_en", err_str, err_str_len);
    std::cerr << "INFO: ad936x_device: bb_dc_offset_tracking_en: " << err_str
              << std::endl;

    rx_cfg.bw_hz = MHZ(1);
    rx_cfg.fs_hz = MHZ(1.6);
    rx_cfg.lo_hz = lo_freq;
    rx_cfg.rfport = "A_BALANCED";
    rx_cfg.hw_gain = hw_gain;

    tx_cfg.bw_hz = MHZ(1.5);
    tx_cfg.fs_hz = MHZ(1.6);
    tx_cfg.lo_hz = GHZ(2.5);
    tx_cfg.rfport = "A";
    tx_cfg.hw_gain = -1;

    std::cerr << "INFO: Configuring AD9361 for streaming..." << std::endl;
    if (!cfg_ad9361_streaming_ch(&rx_cfg, RX, 0)) {
        std::cerr << "ERROR: RX port 0 not found" << std::endl;
        return false;
    }
    if (!cfg_ad9361_streaming_ch(&tx_cfg, TX, 0)) {
        std::cerr << "ERROR: TX port 0 not found" << std::endl;
        return false;
    }

    std::cerr << "INFO: Initializing AD9361 IIO streaming channels..." << std::endl;
    if (!get_ad9361_stream_ch(ctx, RX, rx, 0, &rx0_i)) {
        std::cerr << "ERROR: RX channel I not found" << std::endl;
        return false;
    }

    if (!get_ad9361_stream_ch(ctx, RX, rx, 1, &rx0_q)) {
        std::cerr << "ERROR: RX channel Q not found" << std::endl;
        return false;
    }

    std::cerr << "INFO: Enabling IIO streaming channels..." << std::endl;
    iio_channel_enable(rx0_i);
    iio_channel_enable(rx0_q);

    rxbuf = iio_device_create_buffer(rx, rx_buffer_size, false);
    if (rxbuf == nullptr) {
        std::cerr << "ERROR: Could not create PlutoSDR RX buffer" << std::endl;
        return false;
    }
    iio_buffer_set_blocking_mode(rxbuf, true);

    init_done = true;
    return true;
}

bool ad936x_device::destroy()
{
    if (rxbuf) {
        iio_buffer_destroy(rxbuf);
        rxbuf = nullptr;
    }
    if (rx0_i) {
        iio_channel_disable(rx0_i);
        rx0_i = nullptr;
    }
    if (rx0_q) {
        iio_channel_disable(rx0_q);
        rx0_q = nullptr;
    }
    if (tx0_i) {
        iio_channel_disable(tx0_i);
        tx0_i = nullptr;
    }
    if (tx0_q) {
        iio_channel_disable(tx0_q);
        tx0_q = nullptr;
    }
    if (ctx) {
        iio_context_destroy(ctx);
        ctx = nullptr;
    }
    return true;
}

/* finds AD9361 streaming IIO devices */
bool ad936x_device::get_ad9361_stream_dev(struct iio_context* ctx,
                                          iodev d,
                                          struct iio_device** dev)
{
    switch (d) {
    case TX:
        *dev = iio_context_find_device(ctx, "cf-ad9361-dds-core-lpc");
        return *dev != 0;
    case RX:
        *dev = iio_context_find_device(ctx, "cf-ad9361-lpc");
        return *dev != 0;
    default:
        return false;
    }
}

/* applies streaming configuration through IIO */
bool ad936x_device::cfg_ad9361_streaming_ch(stream_cfg* cfg, iodev type, int chid)
{
    iio_channel* chn = 0;
    char buf[100];

    // Configure phy and lo channels
    std::cerr << "INFO: Acquiring AD9361 phy channel " << chid << std::endl;
    if (!get_phy_chan(type, chid, &chn)) {
        return false;
    }
    wr_ch_str(chn, "rf_port_select", cfg->rfport);
    wr_ch_lli(chn, "rf_bandwidth", cfg->bw_hz);
    wr_ch_lli(chn, "sampling_frequency", cfg->fs_hz);

    if (type == RX) {
        switch (agc_mode) {
        case ad936x_device::AGC_SLOW:
            wr_ch_str(chn, "gain_control_mode", "slow_attack");
            break;

        case ad936x_device::AGC_FAST:
            wr_ch_str(chn, "gain_control_mode", "fast_attack");
            break;

        case ad936x_device::MANUAL:
            wr_ch_str(chn, "gain_control_mode", "manual");
            wr_ch_lli(chn, "hardwaregain", cfg->hw_gain);
            break;
        }

        iio_channel_attr_read(chn, "gain_control_mode", buf, 100);
        std::cerr << "INFO: ad936x_device: RX chain gain_control_mode: " << buf
                  << std::endl;
    }

    // Configure LO channel
    std::cerr << "INFO: Acquiring AD936x " << (type == TX ? "TX" : "RX")
              << " LO channel..." << std::endl;
    if (!get_lo_chan(type, &chn)) {
        return false;
    }
    wr_ch_lli(chn, "frequency", cfg->lo_hz);
    return true;
}

/* finds AD9361 phy IIO configuration channel with id chid */
bool ad936x_device::get_phy_chan(iodev d, int chid, iio_channel** chn)
{
    switch (d) {
    case RX:
        *chn = iio_device_find_channel(phy, get_ch_name("voltage", chid), false);
        return *chn != NULL;
    case TX:
        *chn = iio_device_find_channel(phy, get_ch_name("voltage", chid), true);
        return *chn != NULL;
    default:
        return false;
    }
}

/* returns ad9361 phy device */
iio_device* ad936x_device::get_ad9361_phy(iio_context* ctx)
{
    iio_device* dev = iio_context_find_device(ctx, "ad9361-phy");
    // IIO_ENSURE(dev && "No ad9361-phy found");
    return dev;
}

/* helper function generating channel names */
char* ad936x_device::get_ch_name(const char* type, int id)
{
    snprintf(tmpstr, sizeof(tmpstr), "%s%d", type, id);
    return tmpstr;
}

/* finds AD9361 streaming IIO channels */
bool ad936x_device::get_ad9361_stream_ch(__notused iio_context* ctx,
                                         iodev d,
                                         iio_device* dev,
                                         int chid,
                                         struct iio_channel** chn)
{
    *chn = iio_device_find_channel(dev, get_ch_name("voltage", chid), d == TX);
    if (!*chn)
        *chn = iio_device_find_channel(dev, get_ch_name("altvoltage", chid), d == TX);
    return *chn != 0;
}

/* check return value of attr_write function */
void ad936x_device::errchk(int v, const char* what)
{
    if (v < 0) {
        std::cerr << "ERROR: " << v << " writing to channel " << what << std::endl
                  << "value may not be supported." << std::endl;
        // shutdown();
    }
}

/* write attribute: double */
void ad936x_device::wr_ch_ld(iio_channel* chn, const char* what, double val)
{
    errchk(iio_channel_attr_write_double(chn, what, val), what);
}

/* write attribute: long long int */
void ad936x_device::wr_ch_lli(iio_channel* chn, const char* what, long long val)
{
    errchk(iio_channel_attr_write_longlong(chn, what, val), what);
}

/* write attribute: string */
void ad936x_device::wr_ch_str(iio_channel* chn, const char* what, const char* str)
{
    errchk(iio_channel_attr_write(chn, what, str), what);
}

/* finds AD9361 local oscillator IIO configuration channels */
bool ad936x_device::get_lo_chan(iodev d, iio_channel** chn)
{
    switch (d) {
        // LO chan is always output, i.e. true
    case RX:
        *chn = iio_device_find_channel(phy, get_ch_name("altvoltage", 0), true);
        return *chn != NULL;
    case TX:
        *chn = iio_device_find_channel(phy, get_ch_name("altvoltage", 1), true);
        return *chn != NULL;
    default:
        return false;
    }
}

void ad936x_device::err_str(int ret)
{
    char buf[256];
    iio_strerror(-ret, buf, sizeof(buf));
    fprintf(stderr, "Error during read: %s\n", buf);
}

void ad936x_device::print_libiio_info()
{
    unsigned major, minor;
    char git_tag[8];

    iio_library_get_version(&major, &minor, git_tag);
    std::cerr << "INFO: PlutoSDR using libiio version: " << major << "." << minor
              << " (git tag: " << git_tag << ")" << std::endl;

    std::cerr << "INFO: libiio compiled with backends: ";

    for (unsigned ii = 0; ii < iio_get_backends_count(); ++ii)
        std::cerr << iio_get_backend(ii) << " ";
    std::cerr << std::endl;
}

iio_context* ad936x_device::autodetect_context(bool rtn)
{
    iio_scan_context* scan_ctx;
    iio_context_info** info;
    iio_context* ctx = NULL;
    unsigned int i;
    ssize_t ret;

    scan_ctx = iio_create_scan_context(nullptr, 0);
    if (!scan_ctx) {
        std::cerr << "ERROR: Unable to create scan context" << std::endl;
        return nullptr;
    }

    ret = iio_scan_context_get_info_list(scan_ctx, &info);
    if (ret < 0) {
        char* err_str = new char[16384];
        iio_strerror(-(int)ret, err_str, 16384);
        std::cerr << "ERROR: Scanning for IIO contexts failed: " << err_str << std::endl;
        delete[] err_str;
        goto err_free_ctx;
    }

    if (ret == 0) {
        std::cerr << "ERROR: No IIO context found." << std::endl;
        goto err_free_info_list;
    }
    if (rtn && ret == 1) {
        std::cerr << "INFO: Using auto-detected IIO context at URI "
                  << iio_context_info_get_uri(info[0]) << std::endl;
        ctx = iio_create_context_from_uri(iio_context_info_get_uri(info[0]));
    } else {
        if (rtn) {
            std::cerr << "ERROR: Multiple IIO contexts found. Please select one using -u"
                      << std::endl;
        } else {
            std::cerr << "INFO: Available IIO contexts:" << std::endl;
        }
        for (i = 0; i < (size_t)ret; ++i) {
            std::cerr << "\t" << i << ": " << iio_context_info_get_description(info[i])
                      << " " << iio_context_info_get_uri(info[i]) << std::endl;
        }
    }

err_free_info_list:
    iio_context_info_list_free(info);
err_free_ctx:
    iio_scan_context_destroy(scan_ctx);

    return ctx;
}

std::string ad936x_device::get_rssi_str()
{
    std::lock_guard<std::mutex> lock(mutex);
    rssi_buf[rssi_buf_len - 1] = 0;
    return rssi_buf;
}

std::string ad936x_device::get_hwgain_str()
{
    std::lock_guard<std::mutex> lock(mutex);
    hwgain_buf[hwgain_buf_len - 1] = 0;
    return hwgain_buf;
}
